package qldacnpm10.vipbooking.database.DBHelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import qldacnpm10.vipbooking.database.Model.Booking;
import qldacnpm10.vipbooking.database.Model.Promotion;
import qldacnpm10.vipbooking.database.Model.Table;

/**
 * Created by WELCOME on 11/20/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "VIPBooking";

    // Table Names
    private static final String TABLE_TABLE = "tables";
    private static final String TABLE_BOOKING = "bookings";
    private static final String TABLE_PROMOTION = "promotions";
    //private static final String TABLE_TABLE_BOOKING = "table_booking";

    // Common column names
    private static final String KEY_ID = "id";

    // TABLES Table - column names
    private static final String KEY_TABLE_NAME = "name";
    private static final String KEY_TABLE_CAPACITY = "capacity";

    // BOOKING Table - column names
    private static final String KEY_BOOK_CUSTOMER = "customer";
    private static final String KEY_BOOK_PHONENUMBER = "phone";
    private static final String KEY_BOOK_TABLE = "table";
    private static final String KEY_BOOK_TIME = "time";

    // PROMOTIONS Table - column names
    private static final String KEY_PROMOTION_TITLE = "tiltle";
    private static final String KEY_PROMOTION_DESCRIPTION = "description";
    private static final String KEY_PROMOTION_START = "start_time";
    private static final String KEY_PROMOTION_END = "end_time";

/*    // TABLE_BOOKING Table - column names
    private static final String KEY_TABLE_ID = "table_id";
    private static final String KEY_BOOKING_ID = "booking_id";*/

    // Table Create Statements
    // TABLES table create statement
    private static final String CREATE_TABLE_TABLE = "CREATE TABLE "
            + TABLE_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_TABLE_NAME + " TEXT,"
            + KEY_TABLE_CAPACITY + " INTEGER" + ")";

    // BOOKINGS table create statement
    private static final String CREATE_TABLE_BOOKING = "CREATE TABLE " + TABLE_BOOKING
            + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_BOOK_CUSTOMER + " TEXT,"
            + KEY_BOOK_PHONENUMBER + " TEXT,"
            + KEY_BOOK_TABLE + " TEXT,"
            + KEY_BOOK_TIME + " DATETIME" + ")";

    // PROMOTIONS table create statement
    private static final String CREATE_TABLE_PROMOTION = "CREATE TABLE " + TABLE_PROMOTION
            + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_PROMOTION_TITLE + " TEXT,"
            + KEY_PROMOTION_DESCRIPTION + " TEXT,"
            + KEY_PROMOTION_START + " DATETIME,"
            + KEY_PROMOTION_END + " DATETIME" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TABLE);
        db.execSQL(CREATE_TABLE_BOOKING);
        db.execSQL(CREATE_TABLE_PROMOTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROMOTION);

        // create new tables
        onCreate(db);
    }


    //Table BOOKING
    public long createBooking(Booking book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_BOOK_CUSTOMER, book.getCustomer());
        values.put(KEY_BOOK_PHONENUMBER, book.getPhoneNumber());
        values.put(KEY_BOOK_TABLE, book.getTable());
        values.put(KEY_BOOK_TIME, book.getTime());

        long book_id = db.insert(TABLE_BOOKING, null, values);
        return book_id;
    }

    public Booking getBooking(long book_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKING + " WHERE " + KEY_ID + " = " + book_id;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null)
            c.moveToFirst();
        Booking b = new Booking();
        b.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        b.setCustomer((c.getString(c.getColumnIndex(KEY_BOOK_CUSTOMER))));
        b.setPhoneNumber(c.getString(c.getColumnIndex(KEY_BOOK_PHONENUMBER)));
        b.setTable(c.getString(c.getColumnIndex(KEY_BOOK_TABLE)));
        b.setTime(c.getString(c.getColumnIndex(KEY_BOOK_TIME)));

        return b;
    }

    public List<Booking> getAllTableBooking(String table_name) {
        List<Booking> bookings = new ArrayList<Booking>();
        String selectQuery;
        if (table_name != "Null") {
            selectQuery = "SELECT  * FROM " + TABLE_BOOKING + " b, "
                    + TABLE_TABLE + " t  WHERE b."
                    + KEY_BOOK_TABLE + " = '" + table_name + "'" + " AND b." + KEY_BOOK_TABLE
                    + " = " + "t." + KEY_TABLE_NAME;
        }
        else selectQuery = "SELECT  * FROM " + TABLE_BOOKING;
        Log.e(LOG, selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Booking b = new Booking();
                b.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                b.setCustomer(c.getString(c.getColumnIndex(KEY_BOOK_CUSTOMER)));
                b.setPhoneNumber(c.getString(c.getColumnIndex(KEY_BOOK_PHONENUMBER)));
                b.setTable(c.getString(c.getColumnIndex(KEY_BOOK_TABLE)));
                b.setTime(c.getString(c.getColumnIndex(KEY_BOOK_TIME)));
                bookings.add(b);
            } while (c.moveToNext());
        }
        return bookings;
    }

    public int updateBooking(Booking b) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BOOK_CUSTOMER, b.getCustomer());
        values.put(KEY_BOOK_PHONENUMBER, b.getPhoneNumber());
        values.put(KEY_BOOK_TABLE, b.getTable());
        values.put(KEY_BOOK_TIME, b.getTime());

        // updating row
        return db.update(TABLE_BOOKING, values, KEY_ID + " = ?",
                new String[] { String.valueOf(b.getId()) });
    }

    public void deleteBooking(long book_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOOKING, KEY_ID + " = ?",
                new String[]{String.valueOf(book_id)});
    }

    //Table TABLES
    public long createTable(Table table) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TABLE_NAME, table.getName());
        values.put(KEY_TABLE_CAPACITY, table.getCapacity());

        // insert row
        long table_id = db.insert(TABLE_TABLE, null, values);
        return table_id;
    }

    public Table getTable(long table_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_TABLE + " WHERE " + KEY_ID + " = " + table_id;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null)
            c.moveToFirst();
        Table t = new Table();
        t.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        t.setName((c.getString(c.getColumnIndex(KEY_TABLE_NAME))));
        t.setCapacity(c.getInt(c.getColumnIndex(KEY_TABLE_CAPACITY)));

        return t;
    }

    public List<Table> getAllTables() {
        List<Table> tables = new ArrayList<Table>();
        String selectQuery = "SELECT  * FROM " + TABLE_TABLE;
        Log.e(LOG, selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Table t = new Table();
                t.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                t.setName((c.getString(c.getColumnIndex(KEY_TABLE_NAME))));
                t.setCapacity(c.getInt(c.getColumnIndex(KEY_TABLE_CAPACITY)));

                // adding to todo list
                tables.add(t);
            } while (c.moveToNext());
        }
        return tables;
    }

    public int updateTable(Table table) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TABLE_NAME, table.getName());
        values.put(KEY_TABLE_CAPACITY, table.getCapacity());

        return db.update(TABLE_TABLE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(table.getId()) });
    }

    public void deleteTable(Table table, boolean should_delete_all_table_booking) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (should_delete_all_table_booking) {
            List<Booking> allTableBooking = getAllTableBooking(table.getName());
            for (Booking temp : allTableBooking) {
                deleteBooking(temp.getId());
            }
        }
        db.delete(TABLE_TABLE, KEY_ID + " = ?",
                new String[]{String.valueOf((table.getId()))});
    }

    public int changeTableForBooking(long id, String table_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_BOOK_TABLE, table_name);

        return db.update(TABLE_BOOKING, values, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    // Table PROMOTION
    public long createPromotion(Promotion promotion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROMOTION_TITLE, promotion.getTitle());
        values.put(KEY_PROMOTION_DESCRIPTION, promotion.getDescription());
        values.put(KEY_PROMOTION_START, promotion.getStartTime());
        values.put(KEY_PROMOTION_END, promotion.getEndTime());

        long prom_id = db.insert(TABLE_PROMOTION, null, values);
        return prom_id;
    }

    public List<Promotion> getAllPromotion() {
        List<Promotion> proms = new ArrayList<Promotion>();

        String selectQuery = "SELECT  * FROM " + TABLE_PROMOTION;
        Log.e(LOG, selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Promotion p = new Promotion();
                p.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                p.setTitle(c.getString(c.getColumnIndex(KEY_PROMOTION_TITLE)));
                p.setDescription(c.getString(c.getColumnIndex(KEY_PROMOTION_DESCRIPTION)));
                p.setStartTime(c.getString(c.getColumnIndex(KEY_PROMOTION_START)));
                p.setEndTime(c.getString(c.getColumnIndex(KEY_PROMOTION_END)));
                proms.add(p);
            } while (c.moveToNext());
        }
        return proms;
    }

    public int updatePromotion(Promotion promotion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROMOTION_TITLE, promotion.getTitle());
        values.put(KEY_PROMOTION_DESCRIPTION, promotion.getDescription());
        values.put(KEY_PROMOTION_START, promotion.getStartTime());
        values.put(KEY_PROMOTION_END, promotion.getEndTime());

        // updating row
        return db.update(TABLE_PROMOTION, values, KEY_ID + " = ?",
                new String[] { String.valueOf(promotion.getId()) });
    }

    public void deletePromotion(long prom_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PROMOTION, KEY_ID + " = ?",
                new String[]{String.valueOf(prom_id)});
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
