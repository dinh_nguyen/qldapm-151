package qldacnpm10.vipbooking.database.Model;

/**
 * Created by WELCOME on 11/20/2015.
 */
public class Booking {
    int id;
    String Customer;
    String PhoneNumber;
    String Table;
    String Time;

    public Booking() {}

    public Booking(String Customer, String PhoneNumber, String Table, String Time) {
        this.Customer = Customer;
        this.PhoneNumber = PhoneNumber;
        this.Table = Table;
        this.Time = Time;
    }

    public void setId(int id) {this.id = id;}
    public void setCustomer(String Customer) {this.Customer = Customer;}
    public void setPhoneNumber(String PhoneNumber) {this.PhoneNumber = PhoneNumber;}
    public void setTable(String Table) {this.Table = Table;}
    public void setTime(String Time) {this.Time = Time;}

    public int getId() {return this.id;}
    public String getCustomer() {return this.Customer;}
    public String getPhoneNumber() {return this.PhoneNumber;}
    public String getTable() {return this.Table;}
    public String getTime() {return this.Time;}
}
