package qldacnpm10.vipbooking.database.Model;

/**
 * Created by WELCOME on 11/20/2015.
 */
public class Promotion {
    int id;
    String Title;
    String Description;
    String StartTime;
    String EndTime;

    public Promotion() {}
    public Promotion(String Title, String Des) {
        this.Title = Title;
        this.Description = Des;
    }
    public Promotion(String Title, String Des, String Start, String End) {
        this.Title = Title;
        this.Description = Des;
        this.StartTime = Start;
        this.EndTime = End;
    }

    public void setId(int id) {this.id = id;}
    public void setTitle(String Title) {this.Title = Title;}
    public void setDescription(String Des) {this.Description = Des;}
    public void setStartTime(String Start) {this.StartTime = Start;}
    public void setEndTime(String End) {this.EndTime = End;}

    public int getId() {return this.id;}
    public String getTitle() {return this.Title;}
    public String getDescription() {return this.Description;}
    public String getStartTime() {return this.StartTime;}
    public String getEndTime() {return this.EndTime;}
}
