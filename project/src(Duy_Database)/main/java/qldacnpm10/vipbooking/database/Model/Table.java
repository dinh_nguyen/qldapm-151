package qldacnpm10.vipbooking.database.Model;

/**
 * Created by WELCOME on 11/20/2015.
 */
public class Table {
    int id;
    String Name;
    int Capacity;

    public Table() {}

    public Table(String Name, int Capacity) {
        this.Name = Name;
        this.Capacity = Capacity;
    }

    public void setId(int id) {this.id = id;}
    public void setName(String Name) {this.Name = Name;}
    public void setCapacity(int Capacity) {this.Capacity = Capacity;}

    public int getId() {return this.id;}
    public String getName() {return this.Name;}
    public int getCapacity() {return this.Capacity;}
}
