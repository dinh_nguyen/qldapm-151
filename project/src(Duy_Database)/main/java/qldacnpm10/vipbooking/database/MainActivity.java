package qldacnpm10.vipbooking.database;

import qldacnpm10.vipbooking.database.Model.Table;
import qldacnpm10.vipbooking.database.Model.Booking;
import qldacnpm10.vipbooking.database.Model.Promotion;
import qldacnpm10.vipbooking.database.DBHelpers.DatabaseHelper;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(getApplicationContext());

        // Creating tables
        Table table1 = new Table("Window_1",2);
        Table table2 = new Table("Window_2",2);
        Table table3 = new Table("Window_3",2);
        Table table4 = new Table("Window_4",2);
        // Inserting tags in db
        long ta1_id = db.createTable(table1);
        long ta2_id = db.createTable(table2);
        long ta3_id = db.createTable(table3);
        long ta4_id = db.createTable(table4);

        Log.d("Tag Count", "Tag Count: " + db.getAllTables().size());
        
        // Creating Bookings
        Booking booking1 = new Booking("Alex","111","Window_1","111");
        Booking booking2 = new Booking("Alex1","12","Window_1","112");
        Booking booking3 = new Booking("Alex2","113","Window_1","111");

        Booking booking4 = new Booking("Beta","11","Window_2","111");
        Booking booking5 = new Booking("Beta2","11","Window_2","111");
        Booking booking6 = new Booking("Beta3","11","Window_2","111");
        Booking booking7 = new Booking("Beta4","11","Window_2","111");

        Booking booking8 = new Booking("Gam","11","Window_3","111");
        Booking booking9 = new Booking("Gam1","112","Window_3","111");

        Booking booking10 = new Booking("Delt","11","Window_4","111");
        Booking booking11 = new Booking("Delt1","113","Window_4","111");

        // Inserting bookings in db
        // Inserting bookings under "Shopping" Tag
        long booking1_id = db.createBooking(booking1);
        long booking2_id = db.createBooking(booking2);
        long booking3_id = db.createBooking(booking3);

        // Inserting bookings under "Watchlist" Tag
        long booking4_id = db.createBooking(booking4);
        long booking5_id = db.createBooking(booking5);
        long booking6_id = db.createBooking(booking6);
        long booking7_id = db.createBooking(booking7);

        // Inserting bookings under "Important" Tag
        long booking8_id = db.createBooking(booking8);
        long booking9_id = db.createBooking(booking9);

        // Inserting bookings under "Androidhive" Tag
        long booking10_id = db.createBooking(booking10);
        long booking11_id = db.createBooking(booking11);

        Log.e("Todo Count", "Todo count: " + db.getAllTableBooking("Null").size());

        Log.d("Get Tags", "Getting All Tags");
        List<Table> allTable = db.getAllTables();
        for (Table t : allTable) {
            Log.d("Table",t.getId() + " " + t.getName());
        }

        db.closeDB();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
