package hcmut.cse.ttcnpm.group13.restaurantbookingonline;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.SupportMenuInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.Parse;

import java.util.ArrayList;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.adapter.NavigationDrawerListAdapter;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BaseBookingActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ReminderLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ScheduleClient;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.BookingValidationFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.FeedbackFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.HomeFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.NewsListFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.PromotionListFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.HistoryListFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.model.NavigationDrawerItem;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.tinyDB.TinyDB;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.LocaleUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.NetworkUtils;

public class HomeActivity extends AppCompatActivity {

    private Menu optionsMenu;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // navigation draw title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navigationMenuTitles;
    private TypedArray navigationMenuIcons;

    private ArrayList<NavigationDrawerItem> navigationDrawerItems;
    private NavigationDrawerListAdapter adapter;

    // Back pressed second time variable
    private static final int TIME_INTERVAL_BACK_PRESSED = 2000;
    private long mBackPressed;

    // Navigation Drawer Item
    private NavigationDrawerItem mHomeNavigationItem;
    private NavigationDrawerItem mNewsNavigationItem;
    private NavigationDrawerItem mPromotionsNavigationItem;
    private NavigationDrawerItem mHistoryNavigationItem;
    private NavigationDrawerItem mFeedbackNavigationItem;
    private NavigationDrawerItem mValidationNavigationItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        LocaleUtils.updateConfig(this);

//        NotificationManager n = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        mTitle = mDrawerTitle = getTitle();

        // load slide menu items
        navigationMenuTitles = getResources().getStringArray(R.array.navigation_drawer_items);

        // navigation drawer icons from resources
        navigationMenuIcons = getResources().obtainTypedArray(R.array.navigation_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navigationDrawerItems = new ArrayList<NavigationDrawerItem>();

        ActionBar actionbar = getSupportActionBar();
        // enabling action bar app icon and behaving it as toggle button
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                // When clicked to menu icon
                if (newState == DrawerLayout.STATE_SETTLING) {
                    // Closing menu navigation state
                    if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        ActionBar actionbar = getSupportActionBar();
                        if (actionbar != null) {
                            actionbar.setDisplayShowHomeEnabled(true);
                            actionbar.setTitle(" " + mTitle);
                        }
                    }
                    // Opening menu navigation state
                    else {
                        // Update action bar title
                        ActionBar actionbar = getSupportActionBar();
                        if (actionbar != null) {
                            actionbar.setDisplayShowHomeEnabled(false);
                            actionbar.setTitle(" " + mDrawerTitle);
                        }

                        // Update NEW item count and adapter
                        int newsCount = NewsLab.get(getApplicationContext()).getNewsCounterUpdateRecently();
                        mNewsNavigationItem.setCount(newsCount);
                        mNewsNavigationItem.updateCounterVisible();

                        int promotionCount = PromotionsLab.get(getApplicationContext()).getPromotionsCounterUpdateRecently();
                        mPromotionsNavigationItem.setCount(promotionCount);
                        mPromotionsNavigationItem.updateCounterVisible();

                        // Update and sort History Lab
                        HistoryLab.get(getApplicationContext()).updateBookingState();
                        HistoryLab.get(getApplicationContext()).sortDescendingBookedList();

                        int inComingBookingCount = HistoryLab.get(getApplicationContext()).getCounterIsComingBooking();
                        mHistoryNavigationItem.setCount(inComingBookingCount);
                        mHistoryNavigationItem.updateCounterVisible();

                        adapter.notifyDataSetChanged();
                    }
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // If we do not init lab before, init it
        if (!NewsLab.isInit() || !PromotionsLab.isInit() || !HistoryLab.isInit() || !ReminderLab.isInit())
        {
            new InitLabFromServerTask().execute();
        }

        else
        {
            // Number of "NEW" item in menu navigation
            int newsCount = NewsLab.get(getApplicationContext()).getNewsCounterUpdateRecently();
            int promotionCount = PromotionsLab.get(getApplicationContext()).getPromotionsCounterUpdateRecently();

            // Number of "In coming booking session"
            HistoryLab.get(getApplicationContext()).updateBookingState();
            int inComingBookingCount = HistoryLab.get(getApplicationContext()).getCounterIsComingBooking();

            mHomeNavigationItem = new NavigationDrawerItem(navigationMenuTitles[0], navigationMenuIcons.getResourceId(0, -1));
            mNewsNavigationItem = new NavigationDrawerItem(navigationMenuTitles[1], navigationMenuIcons.getResourceId(1, -1), true, newsCount);
            mPromotionsNavigationItem = new NavigationDrawerItem(navigationMenuTitles[2], navigationMenuIcons.getResourceId(2, -1), true, promotionCount);
            mHistoryNavigationItem = new NavigationDrawerItem(navigationMenuTitles[3], navigationMenuIcons.getResourceId(3, -1), true, inComingBookingCount);
            mFeedbackNavigationItem = new NavigationDrawerItem(navigationMenuTitles[4], navigationMenuIcons.getResourceId(4, -1));
            mValidationNavigationItem = new NavigationDrawerItem(navigationMenuTitles[5], navigationMenuIcons.getResourceId(5, -1));

            navigationDrawerItems.add(mHomeNavigationItem);
            navigationDrawerItems.add(mNewsNavigationItem);
            navigationDrawerItems.add(mPromotionsNavigationItem);
            navigationDrawerItems.add(mHistoryNavigationItem);
            navigationDrawerItems.add(mFeedbackNavigationItem);
            navigationDrawerItems.add(mValidationNavigationItem);

            navigationMenuIcons.recycle();

            // setting the navigation drawer list adapter
            adapter = new NavigationDrawerListAdapter(getApplicationContext(), navigationDrawerItems);
            mDrawerList.setAdapter(adapter);
            mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

            if (savedInstanceState == null) {
                // on first time display view for first navigation item
                displayView(0);
            }
        }
    }


    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // display view for selected navigation drawer item
            view.setSelected(true);
            displayView(position);
        }
    }

    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new NewsListFragment();
                break;
            case 2:
                fragment = new PromotionListFragment();
                break;
            case 3:
                fragment = new HistoryListFragment();
                break;
            case 4:
                fragment = new FeedbackFragment();
                break;
            case 5:
                fragment = BookingValidationFragment.newInstance(true);
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (transaction != null) {
                transaction.replace(R.id.frame_container, fragment).commit();
            }

            // update selected item and title
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navigationMenuTitles[position]);

            // update the image
            ActionBar actionbar = getSupportActionBar();
            if (actionbar != null) {
                int id_logo;
                switch (position) {
                    case 0:
                        id_logo = R.drawable.ic_home;
                        break;
                    case 1:
                        id_logo = R.drawable.ic_news_menu;
                        break;
                    case 2:
                        id_logo = R.drawable.ic_promotions_menu;
                        break;
                    case 3:
                        id_logo = R.drawable.ic_history_menu;
                        break;
                    case 4:
                        id_logo = R.drawable.ic_feedback_menu;
                        break;
                    case 5:
                        id_logo = R.drawable.ic_verification_menu;
                        break;
                    default:
                        id_logo = R.drawable.ic_home;
                        break;
                }
                actionbar.setIcon(id_logo);
                // close the drawer
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // toggle navigation drawer on selecting action bar app/icon title

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.booking_menuRefresh:
                Log.d("Menu","onOptionItemSelected In Activity");
                new UpdateDataFromServerTask().execute();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        // If panel in the left is open, closed it
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
        }
        // Back pressed 2 times and it will close your app
        else {
            if (mBackPressed + TIME_INTERVAL_BACK_PRESSED > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getApplicationContext(), R.string.toast_first_time_back_pressed, Toast.LENGTH_SHORT).show();
            }
            mBackPressed = System.currentTimeMillis();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) actionbar.setTitle(" " + title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occured.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    // When booking fragment finish (User successful register booking), this will be call
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            int test = data.getIntExtra(HomeFragment.BOOKING_STATE, -101);
            if (test == BaseBookingActivity.BOOKING_SUCCEED) {
                // Create BookingValidation not from home menu
                Fragment fragment = BookingValidationFragment.newInstance(false);

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                if (transaction != null) {
                    transaction.replace(R.id.frame_container, fragment).commit();
                }

                if (mDrawerList != null) {
                    mDrawerList.setItemChecked(5, true);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        TinyDB tinyDB = new TinyDB(getApplicationContext());

        ArrayList<Object> objectList = new ArrayList<Object>();

        ReminderLab.get(getApplicationContext()).updateBookingState();
        List<BookingSessionInfo> tmpSessionList = ReminderLab.get(getApplicationContext()).getReminderList();

        for(BookingSessionInfo session: tmpSessionList)
        {
            objectList.add(session);
        }

        tinyDB.putListObject(ReminderLab.ARG_LIST_REMINDER_APP, objectList);
        tinyDB.putListObject(ReminderLab.ARG_LIST_REMINDER_BOOT, objectList);

        super.onPause();
    }

    private class RetryDialog extends MyUtils.MyAlertDialog {

        @Override
        public Activity getCurrentActivity() {
            return HomeActivity.this;
        }

        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            new InitLabFromServerTask().execute();
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {
            getCurrentActivity().finish();
        }
    }

    private class UpdateDataFromServerTask extends AsyncTask<Void, Void, Boolean> {
        // progress dialog
        private ProgressDialog mUpdateDataProgressDialog = null;

        private ScheduleClient mScheduleClient = null;

        @Override
        protected void onPreExecute() {
            mUpdateDataProgressDialog = ProgressDialog.show(HomeActivity.this,"Updating","Please wait for a few seconds",true);
            mScheduleClient = new ScheduleClient(HomeActivity.this);
            mScheduleClient.doBindService();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            final Context context = getApplicationContext();
            boolean isConnected = NetworkUtils.isNetworkConnected(context);
            // If can connect to internet, establish server and get data

            if (isConnected) {

                // Init data from server
                NewsLab.get(context).updateDataLocalFromServer(context);
                PromotionsLab.get(context).updateDataLocalFromServer(context);
                HistoryLab.get(context).updateDataLocalFromServer(context);

                // If not finish load news and promotions, keep stay in background
                while (NewsLab.isLoadingData());
                while (PromotionsLab.isLoadingData());
                while (HistoryLab.isLoadingData());
                ReminderLab.get(context).reNewBookingListFromHistory(context);

                List<BookingSessionInfo> reminderList = ReminderLab.get(context).getReminderList();
                for (int i = reminderList.size() - 1; i >= 0; i--)
                {
                    mScheduleClient.setAlarmForNotification(i);
                }
            }
            return isConnected;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mUpdateDataProgressDialog.dismiss();
            mScheduleClient.doUnBindService();
            if (result) {
                int newsCount = NewsLab.get(getApplicationContext()).getNewsCounterUpdateRecently();
                int promotionCount = PromotionsLab.get(getApplicationContext()).getPromotionsCounterUpdateRecently();

                // Number of "In coming booking session"
                HistoryLab.get(getApplicationContext()).updateBookingState();
                int inComingBookingCount = HistoryLab.get(getApplicationContext()).getCounterIsComingBooking();

                Log.d("Menu","Number of history count = " + inComingBookingCount);

                mNewsNavigationItem.setCount(newsCount);
                mNewsNavigationItem.updateCounterVisible();

                mPromotionsNavigationItem.setCount(promotionCount);
                mPromotionsNavigationItem.updateCounterVisible();

                mHistoryNavigationItem.setCount(inComingBookingCount);
                mHistoryNavigationItem.updateCounterVisible();

                adapter.notifyDataSetChanged();
            }
            else
            {
                Toast toast = Toast.makeText(HomeActivity.this,"You must have internet connection to update",Toast.LENGTH_SHORT);
                toast.setMargin(0.0f,0.8f);
                toast.show();
            }
        }
    }

    private class InitLabFromServerTask extends AsyncTask<Void, Void,Boolean> {

        // progress dialog
        private ProgressDialog mInitLabProgressDialog = null;

        @Override
        protected void onPreExecute() {
            mInitLabProgressDialog = ProgressDialog.show(HomeActivity.this,"Connecting Server","Please wait for a few seconds",true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final Context context = getApplicationContext();
            boolean isConnected = NetworkUtils.isNetworkConnected(context);
            // If can connect to internet, establish server and get data

            if (isConnected) {

                // Init data from server
                NewsLab.get(context);
                PromotionsLab.get(context);
                HistoryLab.get(context);

                // If not finish load news and promotions, keep stay in background
                while (NewsLab.isLoadingData());
                while (PromotionsLab.isLoadingData());
                while (HistoryLab.isLoadingData());
                ReminderLab.get(context).reNewBookingListFromHistory(context);
            }
            return isConnected;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mInitLabProgressDialog.dismiss();

            // Number of "NEW" item in menu navigation
            if (result) {
                int newsCount = NewsLab.get(getApplicationContext()).getNewsCounterUpdateRecently();
                int promotionCount = PromotionsLab.get(getApplicationContext()).getPromotionsCounterUpdateRecently();

                // Number of "In coming booking session"
                HistoryLab.get(getApplicationContext()).updateBookingState();
                int inComingBookingCount = HistoryLab.get(getApplicationContext()).getCounterIsComingBooking();

                displayView(0);

                mHomeNavigationItem = new NavigationDrawerItem(navigationMenuTitles[0], navigationMenuIcons.getResourceId(0, -1));
                mNewsNavigationItem = new NavigationDrawerItem(navigationMenuTitles[1], navigationMenuIcons.getResourceId(1, -1), true, newsCount);
                mPromotionsNavigationItem = new NavigationDrawerItem(navigationMenuTitles[2], navigationMenuIcons.getResourceId(2, -1), true, promotionCount);
                mHistoryNavigationItem = new NavigationDrawerItem(navigationMenuTitles[3], navigationMenuIcons.getResourceId(3, -1), true, inComingBookingCount);
                mFeedbackNavigationItem = new NavigationDrawerItem(navigationMenuTitles[4], navigationMenuIcons.getResourceId(4, -1));
                mValidationNavigationItem = new NavigationDrawerItem(navigationMenuTitles[5], navigationMenuIcons.getResourceId(5, -1));

                navigationDrawerItems.add(mHomeNavigationItem);
                navigationDrawerItems.add(mNewsNavigationItem);
                navigationDrawerItems.add(mPromotionsNavigationItem);
                navigationDrawerItems.add(mHistoryNavigationItem);
                navigationDrawerItems.add(mFeedbackNavigationItem);
                navigationDrawerItems.add(mValidationNavigationItem);

                navigationMenuIcons.recycle();

                // setting the navigation drawer list adapter
                adapter = new NavigationDrawerListAdapter(getApplicationContext(), navigationDrawerItems);
                mDrawerList.setAdapter(adapter);
                mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

                mDrawerList.setItemChecked(0, true);
                mDrawerList.setSelection(0);
            }
            else
            {
                AlertDialog retryDialog = new RetryDialog().createMyAlertDialog("No Connection Available","Please press Retry to try again or Exit to exit app","RETRY","EXIT");
                retryDialog.show();
            }
        }
    }
}

