package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseTimeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseTimeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseTimeFragment extends Fragment {
    private static final String TAG = "ChooseTimeFragment";

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    private static final String DIALOG_DATE = "DialogDate";
    private static final String DIALOG_TIME = "DialogTime";

    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_TIME = 1;

    private Button mDateButton = null;
    private Button mTimeButton = null;
    private TextView mTodayTextView = null;

    public ChooseTimeFragment() {
    }

    private void updateDateButton() {
        Calendar mDate = BaseBookingActivity.getCurrentSession(getActivity()).getBookingDateOnly();
        // Update to button text
        if (mDateButton != null) {
            if (mDate != null) {
                mDateButton.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.ENGLISH).format(mDate.getTime()));
            } else {
                mDateButton.setText(getString(R.string.txt_default_date_button));
            }
        }
    }

    private void updateTimeButton() {
        // Update to button text
        Calendar mTime = BaseBookingActivity.getCurrentSession(getActivity()).getBookingTimeOnly();
        if (mTimeButton != null) {
            if (mTime != null) {
                mTimeButton.setText(new SimpleDateFormat("kk:mm", Locale.ENGLISH).format(mTime.getTime()));
            } else {
                mTimeButton.setText(getString(R.string.txt_default_time_button));
            }
        }
    }

    private void updateToday() {
        if (mTodayTextView != null) {
            Calendar today = Calendar.getInstance();
            mTodayTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.ENGLISH).format(today.getTime()));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_choose_time, container, false);
        if (rootView != null) {
            mDateButton = (Button) rootView.findViewById(R.id.btnBookingDate);
            updateDateButton();
            if (mDateButton != null) {
                mDateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        FragmentManager manager = getFragmentManager();
                        Calendar mDate = BaseBookingActivity.getCurrentSession(getActivity()).getBookingDateOnly();
                        DatePickerFragment dialog = DatePickerFragment.newInstance(mDate);
                        dialog.setTargetFragment(ChooseTimeFragment.this, REQUEST_DATE);
                        dialog.show(manager, DIALOG_DATE);
                    }
                });
            }

            mTimeButton = (Button) rootView.findViewById(R.id.btnBookingTime);
            updateTimeButton();
            if (mTimeButton != null) {
                mTimeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        Calendar mTime = BaseBookingActivity.getCurrentSession(getActivity()).getBookingTimeOnly();
                        TimePickerFragment dialog = TimePickerFragment.newInstance(mTime);
                        dialog.setTargetFragment(ChooseTimeFragment.this, REQUEST_TIME);
                        dialog.show(manager, DIALOG_TIME);
                    }
                });
            }

            mTodayTextView = (TextView) rootView.findViewById(R.id.txtToday);
            updateToday();
        }
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_DATE) {
            Calendar date = (Calendar) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            BaseBookingActivity.getCurrentSession(getActivity()).setBookingDateOnly(date);
            updateDateButton();
        } else if (requestCode == REQUEST_TIME) {
            Calendar time = (Calendar) data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
            BaseBookingActivity.getCurrentSession(getActivity()).setBookingTimeOnly(time);
            updateTimeButton();
        }
    }
}
