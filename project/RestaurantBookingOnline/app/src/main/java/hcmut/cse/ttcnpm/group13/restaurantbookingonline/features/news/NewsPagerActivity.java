package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.base.MyFeatureActivity;

/**
 * Created by dinh on 11/13/2015.
 */
public class NewsPagerActivity extends MyFeatureActivity {

    private static final String EXTRA_NEWS_ID =
            "hcmut.cse.ttcnpm.group13.restaurantbookingonline.news_id";

    private ViewPager mNewsViewPager;
    private List<News> mNewsList;

    private HashMap<String, Integer> mapNewsListUUID;

    public static Intent newIntent(Context packageContext, String newsId) {
        Intent intent = new Intent(packageContext, NewsPagerActivity.class);
        intent.putExtra(EXTRA_NEWS_ID, newsId);
        return intent;
    }

    @Override
    public void setMyFeaturesTitle() {
        setTitle("  " + getString(R.string.txt_news_detail_title));
    }

    @Override
    public void setMyFeaturesIcon() {
        getSupportActionBar().setIcon(R.drawable.ic_news_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_pager);

        mNewsList = NewsLab.get(this).getNewsList();
        mapNewsListUUID = new HashMap<String, Integer>();
        // Map News UUID to Id
        for (int i = 0; i < mNewsList.size(); i++) {
            mapNewsListUUID.put(mNewsList.get(i).getId(), i);
        }

        mNewsViewPager = (ViewPager) findViewById(R.id.activity_news_pager);
        if (mNewsViewPager != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            mNewsViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                @Override
                public Fragment getItem(int position) {
                    News news = mNewsList.get(position);
                    return NewsDetailFragment.newInstance(news.getId());
                }

                @Override
                public int getCount() {
                    return mNewsList.size();
                }
            });

            mNewsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    News news = mNewsList.get(position);
                    if (news != null) {
                        news.setIsNew(false);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            Intent curIntent = getIntent();
            if (curIntent != null) {
                String newsId = (String) curIntent.getSerializableExtra(EXTRA_NEWS_ID);
                int currentPosition = mapNewsListUUID.get(newsId);
                News news = mNewsList.get(currentPosition);
                if (news != null) {
                    news.setIsNew(false);
                }
                mNewsViewPager.setCurrentItem(currentPosition);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
