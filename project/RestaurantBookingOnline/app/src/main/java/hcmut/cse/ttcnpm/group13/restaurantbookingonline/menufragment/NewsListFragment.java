package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.Calendar;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.cache.PreCachingLayoutManager;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsPagerActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.DeviceUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/6/2015.
 */
public class NewsListFragment extends Fragment {

    public NewsListFragment() {
    }

    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    private RecyclerView mNewsRecyclerView;
    private NewsAdapter mNewsAdapter;

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    private Calendar todayCalendar = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.booking_menuRefresh:
                Log.d("Menu", "onOptionItemSelected In NewsFragment");
                new UpdateNewsListTask().execute();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_lists, container, false);
        mNewsRecyclerView = (RecyclerView) rootView.findViewById(R.id.news_recycler_view);
        if (mNewsRecyclerView != null) {
            PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
            layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
            mNewsRecyclerView.setLayoutManager(layoutManager);
            updateUI();
        }

        return rootView;
    }

    private void updateUI() {
        if (mNewsAdapter == null) {
            NewsLab newsLab = NewsLab.get(getActivity());
            List<News> newsList = newsLab.getNewsList();
            mNewsAdapter = new NewsAdapter(newsList);
            mNewsRecyclerView.setAdapter(mNewsAdapter);
        }
        else
        {
            NewsLab newsLab = NewsLab.get(getActivity());
            List<News> newsList = newsLab.getNewsList();
            mNewsAdapter.notifyDataSetChanged();
            mNewsAdapter.notifyItemRangeChanged(0, newsList.size());
        }
    }

    private class UpdateNewsListTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            while(NewsLab.isLoadingData());
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Log.d("Menu", "Adapter In NewsFragment Updated");
            updateUI();
        }
    }

    private class NewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private News mNews;

        private ImageView mImageView;
        private TextView mTitleTextView;
        private TextView mDescriptionTextView;
        private TextView mDateTextView;
        private TextView mNewTextView;

        private View mBackground;

        public View getBackground() {
            return mBackground;
        }

        public void setBackground(View background) {
            mBackground = background;
        }

        public NewsHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mBackground = itemView;

            mImageView = (ImageView) itemView.findViewById(R.id.list_item_news_icon_image_view);
            mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_news_title_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_news_date_text_view);
            mNewTextView = (TextView) itemView.findViewById(R.id.list_item_news_is_new_text_view);
            mDescriptionTextView = (TextView)itemView.findViewById(R.id.list_item_news_description_text_view);
        }

        public void bindNews(News news) {
            mNews = news;
            //mImageView.setImageResource(mNews.getIconId());
            Picasso.with(getContext())
                    .load(mNews.getIconId())
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(mImageView);
            mTitleTextView.setText(mNews.getTitle());
            mDescriptionTextView.setText(mNews.getDescription());

            StringBuilder compareAuthorDateStr = MyUtils.getStringAfterCompare(mNews.getDate(), todayCalendar);

            // If find second in string
            if (compareAuthorDateStr.indexOf("second") != -1) {
                compareAuthorDateStr = new StringBuilder("A minute ago");
            }

            mDateTextView.setText(compareAuthorDateStr);

            if (mNews.isNew()) {
                mNewTextView.setVisibility(View.VISIBLE);
            } else {
                mNewTextView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View v) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            // highlight selected item
            int pos = getAdapterPosition();

            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
                mBackground.setSelected(false);
            } else {
                selectedItems.put(pos, true);
                mBackground.setSelected(true);
            }

            // Set new label invisible when clicked
            Intent intent = NewsPagerActivity.newIntent(getActivity(), mNews.getId());
            startActivity(intent);
        }
    }

    private class NewsAdapter extends RecyclerView.Adapter<NewsHolder> {

        private List<News> mNewsList;

        public NewsAdapter(List<News> newsList) {
            mNewsList = newsList;
        }

        @Override
        public void onBindViewHolder(NewsHolder holder, int position) {
            News news = mNewsList.get(position);
            holder.bindNews(news);
            holder.mBackground.setSelected(selectedItems.get(position, false));
        }

        @Override
        public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_news, parent, false);
            NewsHolder holder = new NewsHolder(view);
            return holder;
        }

        @Override
        public int getItemCount() {
            return mNewsList.size();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
}