package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news;

import android.content.Context;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator.NewsDescendingComparator;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/13/2015.
 */
public class NewsLab {

    private static boolean isLoadingData = false;

    private static boolean isInit = false;

    private final long MILLIS_SECOND_IN_WEEK = 1000 * 60 * 60 * 24 * 7;

    private static NewsLab sNewsLab;

    // List object from parse server
    List<ParseObject> mNewsParseObjectList;

    // Data in app
    private List<News> mNewsList;

    public static NewsLab get(Context context) {
        if (sNewsLab == null) {
            sNewsLab = new NewsLab(context);
            isInit = true;
        }
        return sNewsLab;
    }

    private NewsLab(Context context) {
        isInit = true;
        mNewsList = new ArrayList<>();
        updateDataLocalFromServer(context);
    }

    public List<News> getNewsList() {
        return mNewsList;
    }

    public static boolean isLoadingData()
    {
        return isLoadingData;
    }

    public static boolean isInit() {
        return isInit;
    }

    public News getNews(String id) {
        for (News news : mNewsList) {
            if (news.getId().equals(id)) {
                return news;
            }
        }
        return null;
    }

    public int getNewsCounterUpdateRecently() {
        int count = 0;
        for (News news : mNewsList) {
            if (news.isNew()) count += 1;
        }
        return count;
    }

    public void updateDataLocalFromServer(final Context aContext) {
        isLoadingData = true;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("NewsInfor");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                ParseObject.unpinAllInBackground("NewsInformationParse", new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        ParseObject.pinAllInBackground("NewsInformationParse", objects);
                        updateDataFromCache(aContext);
                    }
                });
            }
        });
    }

    public void updateDataFromCache(final Context aContext) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("NewsInfor");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                mNewsParseObjectList = objects;
                if (objects != null) {
                    updateDataLocal(aContext);
                    isLoadingData = false;
                }
            }
        });
    }


    public void updateDataLocal(Context aContext) {

        List<News> compareList = new ArrayList<>();

        // Clear old and store to another list to compare
        if (mNewsList != null) {
            compareList.addAll(mNewsList);
            mNewsList.clear();
        }
        long todayInMillis = Calendar.getInstance().getTimeInMillis();
        for (ParseObject newsRecord: mNewsParseObjectList) {
            ParseFile image = (ParseFile) newsRecord.get("Image");
            Calendar postDate = Calendar.getInstance();
            long timeMillisDateFromServer = newsRecord.getDate("PostDate").getTime();

            String id = newsRecord.getObjectId();
            String imagePath = image.getUrl();
            String newsLink = newsRecord.getString("Link");
            String author = newsRecord.getString("Author");
            String title = newsRecord.getString("Title");
            String description = newsRecord.getString("Description");
            String content = newsRecord.getString("Content");

            boolean isNew = true;
            if ((todayInMillis - timeMillisDateFromServer) > MILLIS_SECOND_IN_WEEK) {
                isNew = false;
            }

            postDate.setTimeInMillis(timeMillisDateFromServer);

            News news = new News(id, imagePath, newsLink, author, title, content, description, postDate, isNew);

            int findIndex = compareList.indexOf(news);
            if (findIndex != -1)
            {
                news.setIsNew(compareList.get(findIndex).isNew());
            }

            mNewsList.add(news);
        }

        compareList.clear();

        Collections.sort(mNewsList, new NewsDescendingComparator());
    }
}
