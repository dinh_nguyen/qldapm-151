package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FillGuestInformationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FillGuestInformationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FillGuestInformationFragment extends Fragment {
    public FillGuestInformationFragment() {
    }

    private EditText mEditTextGuestName;
    private EditText mEditTextPhoneNumber;
    private EditText mEditTextEmail;
    private EditText mEditTextNote;

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fill_guest_information, container, false);
        if (rootView != null) {
            mEditTextGuestName = (EditText) rootView.findViewById(R.id.editTxtGuestName);
            if (mEditTextGuestName != null) {
                mEditTextGuestName.setText(BaseBookingActivity.getCurrentSession(getActivity()).getName());
                mEditTextGuestName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        BaseBookingActivity.getCurrentSession(getActivity()).setName(mEditTextGuestName.getText().toString());
                    }
                });
            }

            mEditTextPhoneNumber = (EditText) rootView.findViewById(R.id.editTxtPhoneNumber);
            if (mEditTextPhoneNumber != null) {
                mEditTextPhoneNumber.setText(BaseBookingActivity.getCurrentSession(getActivity()).getPhoneNumber());
                mEditTextPhoneNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        //BaseBookingActivity.mPhoneNumber = mEditTextPhoneNumber.getText().toString();
                        BaseBookingActivity.getCurrentSession(getActivity()).setPhoneNumber(mEditTextPhoneNumber.getText().toString());
                    }
                });
            }

            mEditTextEmail = (EditText) rootView.findViewById(R.id.editTxtEmail);
            if (mEditTextEmail != null) {
                mEditTextEmail.setText(BaseBookingActivity.getCurrentSession(getActivity()).getEmail());
                mEditTextEmail.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        //BaseBookingActivity.mEmail = mEditTextEmail.getText().toString();
                        BaseBookingActivity.getCurrentSession(getActivity()).setEmail(mEditTextEmail.getText().toString());
                    }
                });

                // Set when press enter, auto pop up dialog from note edit text
                mEditTextEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_NEXT) {
                            v.setPressed(true);
                            showNoteDialog();
                        }
                        return false;
                    }
                });
            }

            mEditTextNote = (EditText) rootView.findViewById(R.id.editTxtNote);
            if (mEditTextNote != null) {
                mEditTextNote.setText(BaseBookingActivity.getCurrentSession(getActivity()).getNote());
                mEditTextNote.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        BaseBookingActivity.getCurrentSession(getActivity()).setNote(mEditTextNote.getText().toString());
                    }
                });

                mEditTextNote.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                                return true;
                            }
                            mEditTextNote.requestFocus();
                            mLastClickTime = SystemClock.elapsedRealtime();
                            v.setPressed(true);
                            showNoteDialog();
                        }
                        // return true will not raise event onclick
                        return true;
                    }
                });
            }
        }
        return rootView;
    }

    private void showNoteDialog() {
        NoteInputDialog currentDialog = new NoteInputDialog();
        AlertDialog alertDialog = currentDialog.createMyAlertDialog(R.string.txt_fill_guest_note_dialog_title, R.string.txt_fill_guest_note_dialog_message, R.string.txt_OK, R.string.txt_Cancel);

        EditText currentEditText = currentDialog.getDialogEditText();
        currentEditText.setText(BaseBookingActivity.getCurrentSession(getActivity()).getNote());

        alertDialog.show();
    }

    private class NoteInputDialog extends MyUtils.MyAlertDialog {

        private EditText mNoteDialogEditText = null;

        public EditText getDialogEditText() {
            return mNoteDialogEditText;
        }

        @Override
        public Activity getCurrentActivity() {
            return getActivity();
        }

        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            if (mNoteDialogEditText != null && mEditTextNote != null) {
                mEditTextNote.setText(mNoteDialogEditText.getText().toString());
            }
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {

        }

        @Override
        public AlertDialog createMyAlertDialog(String aTitle, String aMessage, String txtPositiveBtn, String txtNegativeBtn) {
            AlertDialog alertDialog = super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            final View dialogView = layoutInflater.inflate(R.layout.dialog_note, null);
            alertDialog.setView(dialogView);

            mNoteDialogEditText = (EditText) dialogView.findViewById(R.id.noteDialogEditText);
            mNoteDialogEditText.requestFocus();

            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            return alertDialog;
        }
    }
}
