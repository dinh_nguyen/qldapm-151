package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
//import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.base.MyFeatureActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingFullInfoFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/20/2015.
 */
public class HistoryPagerActivity extends MyFeatureActivity {

    private static final String EXTRA_HISTORY_ID =
            "hcmut.cse.ttcnpm.group13.restaurantbookingonline.history_id";

    private TextView mHeaderTextView = null;

    private ViewPager mHistoryViewPager;
    private List<BookingSessionInfo> mBookedList;

    private HashMap<String, Integer> mapBookedListUUID;

    public static Intent newIntent(Context packageContext, String historyId) {
        Intent intent = new Intent(packageContext, HistoryPagerActivity.class);
        intent.putExtra(EXTRA_HISTORY_ID, historyId);
        return intent;
    }

    @Override
    public void setMyFeaturesTitle() {
        setTitle("  " + getString(R.string.txt_booking_information_title));
    }

    @Override
    public void setMyFeaturesIcon() {
        getSupportActionBar().setIcon(R.drawable.ic_booking_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_pager);

        mHeaderTextView = (TextView) findViewById(R.id.txtHistoryTitle);

        mBookedList = HistoryLab.get(this).getBookedList();
        mapBookedListUUID = new HashMap<String, Integer>();
        // Map History String to Id
        for (int i = 0; i < mBookedList.size(); i++) {
            mapBookedListUUID.put(mBookedList.get(i).getId(), i);
        }

        mHistoryViewPager = (ViewPager) findViewById(R.id.activity_history_pager);
        if (mHistoryViewPager != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            mHistoryViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                @Override
                public Fragment getItem(int position) {
                    BookingSessionInfo bookedSession = mBookedList.get(position);
                    BookingFullInfoFragment fragment = BookingFullInfoFragment.newInstance(bookedSession);
                    fragment.setVisibleFooterTextView(false);
                    return fragment;
                }

                @Override
                public int getCount() {
                    return mBookedList.size();
                }


            });

            mHistoryViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    BookingSessionInfo bookedSession = mBookedList.get(position);
                    mHeaderTextView.setText(bookedSession.getName());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

        Intent curIntent = getIntent();
        if (curIntent != null) {
            String historyId = (String) curIntent.getSerializableExtra(EXTRA_HISTORY_ID);
            int currentPosition = mapBookedListUUID.get(historyId);
            // When the pos is 0, viewpager has a bug not load pager
            if (currentPosition == 0)
            {
                mHeaderTextView.setText(mBookedList.get(0).getName());
            }
            mHistoryViewPager.setCurrentItem(currentPosition);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
