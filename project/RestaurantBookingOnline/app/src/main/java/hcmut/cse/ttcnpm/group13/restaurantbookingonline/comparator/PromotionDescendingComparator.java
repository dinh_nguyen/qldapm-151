package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.Promotion;

/**
 * Created by dinh on 11/22/2015.
 */
public class PromotionDescendingComparator implements Comparator<Promotion> {
    @Override
    public int compare(Promotion lhs, Promotion rhs) {
        return new PromotionAscendingComparator().compare(rhs, lhs);
    }
}
