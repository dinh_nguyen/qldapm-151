package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions;

import android.content.Context;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator.PromotionDescendingComparator;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/15/2015.
 */
public class PromotionsLab {

    private static boolean isLoadingData = false;

    private static boolean isInit = false;

    private final long MILLIS_SECOND_IN_WEEK = 1000 * 60 * 60 * 24 * 7;

    // List object from parse server
    List<ParseObject> mPromotionParseObjectList;

    private static PromotionsLab mPromotionsLab;

    private List<Promotion> mPromotionsList;

    private PromotionsLab(Context aContext) {
        isInit = true;
        mPromotionsList = new ArrayList<>();
        updateDataLocalFromServer(aContext);
    }

    public static PromotionsLab get(Context aContext) {
        if (mPromotionsLab == null) {
            mPromotionsLab = new PromotionsLab(aContext);
        }
        return mPromotionsLab;
    }

    public List<Promotion> getPromotionsList() {
        return mPromotionsList;
    }

    public static boolean isLoadingData()
    {
        return isLoadingData;
    }

    public static boolean isInit() {
        return isInit;
    }

    public Promotion getPromotion(String id) {
        for (Promotion promotion : mPromotionsList) {
            if (promotion.getId().equals(id)) {
                return promotion;
            }
        }
        return null;
    }

    public int getPromotionsCounterUpdateRecently() {
        int count = 0;
        for (Promotion promotion : mPromotionsList) {
            if (promotion.isNew()) {
                count += 1;
            }
        }
        return count;
    }

    public void updateDataLocalFromServer(final Context aContext) {
        isLoadingData = true;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PromotionInfor");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                ParseObject.unpinAllInBackground("PromotionInformationParse", new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        ParseObject.pinAllInBackground("PromotionInformationParse", objects);
                        updateDataFromCache(aContext);
                    }
                });
            }
        });
    }

    public void updateDataFromCache(final Context aContext) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PromotionInfor");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                mPromotionParseObjectList = objects;
                if (objects != null) {
                    updateDataLocal(aContext);
                    isLoadingData = false;
                }
            }
        });
    }

    public void updateDataLocal(Context aContext) {

        List<Promotion> compareList = new ArrayList<>();

        // Clear old
        if (mPromotionsList != null) {
            compareList.addAll(mPromotionsList);
            mPromotionsList.clear();
        }

        long todayInMillis = Calendar.getInstance().getTimeInMillis();

        for (ParseObject promotionRecord : mPromotionParseObjectList) {
            ParseFile image = (ParseFile) promotionRecord.get("Image");
            Calendar postDate = Calendar.getInstance();
            Calendar expiredDate = Calendar.getInstance();
            long timeMillisDateFromServer = promotionRecord.getDate("PostDate").getTime();
            long timeMillisExpiredDateFromServer = promotionRecord.getDate("ExpiredDate").getTime();

            String id = promotionRecord.getObjectId();
            String imagePath = image.getUrl();
            String link = promotionRecord.getString("Link");
            String author = promotionRecord.getString("Author");
            String title = promotionRecord.getString("Title");
            String description = promotionRecord.getString("Description");
            String content = promotionRecord.getString("Content");

            postDate.setTimeInMillis(timeMillisDateFromServer);
            expiredDate.setTimeInMillis(timeMillisExpiredDateFromServer);

            boolean isNew = true;
            if ((todayInMillis - timeMillisDateFromServer) > MILLIS_SECOND_IN_WEEK) {
                isNew = false;
            }

            Promotion promotion = new Promotion(id, imagePath, link, author, title, content, description, postDate, expiredDate, isNew);

            int findIndex = compareList.indexOf(promotion);
            if (findIndex != -1)
            {
                promotion.setIsNew(compareList.get(findIndex).isNew());
            }

            mPromotionsList.add(promotion);
        }

        compareList.clear();

        Collections.sort(mPromotionsList, new PromotionDescendingComparator());
    }
}
