package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/22/2015.
 */
public class BookingSessionDescendingComparator implements Comparator<BookingSessionInfo> {
    @Override
    public int compare(BookingSessionInfo lhs, BookingSessionInfo rhs) {
        return new BookingSessionAscendingComparator().compare(rhs, lhs);
    }
}
