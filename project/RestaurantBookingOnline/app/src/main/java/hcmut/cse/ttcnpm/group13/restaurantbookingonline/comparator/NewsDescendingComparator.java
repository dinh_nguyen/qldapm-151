package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;

/**
 * Created by dinh on 11/22/2015.
 */
public class NewsDescendingComparator implements Comparator<News> {
    @Override
    public int compare(News lhs, News rhs) {
        return new NewsAscendingComparator().compare(rhs, lhs);
    }
}
