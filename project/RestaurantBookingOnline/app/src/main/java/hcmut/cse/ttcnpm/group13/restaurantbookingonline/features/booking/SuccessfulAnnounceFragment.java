package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuccessfulAnnounceFragment extends Fragment {


    public SuccessfulAnnounceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_successful_announce, container, false);
        return rootView;
    }
}
