package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Calendar;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.tinyDB.TinyDB;

/**
 * Created by dinh on 11/22/2015.
 */
public class AlarmBootService extends Service {

    private final IBinder mBinder = new AlarmBootBinder();

    private ArrayList<BookingSessionInfo> mListBookingSession = new ArrayList<BookingSessionInfo>();

    public class AlarmBootBinder extends Binder {
        AlarmBootService getService() {
            return AlarmBootService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        requestAllRemindNotifyInBoot(this);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void requestAllRemindNotifyInBoot(Context aContext) {

        ScheduleService scheduleService = new ScheduleService();
        TinyDB tinyDB = new TinyDB(aContext);
        ArrayList<Object> listFromDB = tinyDB.getListObject(ReminderLab.ARG_LIST_REMINDER_BOOT, BookingSessionInfo.class);

        // Update state booking depend on today
        updateBookingState(listFromDB);

        scheduleService.setAlarmBoot(aContext, listFromDB);
    }

    public void updateBookingState(ArrayList<Object> aList) {
        long todayMillis = Calendar.getInstance().getTimeInMillis();

        for (Object object : aList) {
            BookingSessionInfo session = (BookingSessionInfo)object;
            if (session != null) {
                int state = session.getState();
                if (state != BookingSessionInfo.EXPIRED) {
                    long sessionMillis = session.getRealFullBookingDate().getTimeInMillis();
                    if (todayMillis > sessionMillis) {
                        session.setState(BookingSessionInfo.EXPIRED);
                    }
                }
            }
        }

        for (int i = aList.size() - 1; i >= 0; i--) {
            BookingSessionInfo session = (BookingSessionInfo)aList.get(i);
            if (session != null) {
                if (session.getState() == BookingSessionInfo.EXPIRED) {
                    aList.remove(i);
                }
            }
        }
    }
}
