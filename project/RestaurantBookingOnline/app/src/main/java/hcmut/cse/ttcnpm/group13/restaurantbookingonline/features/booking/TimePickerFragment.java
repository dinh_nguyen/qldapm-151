package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/10/2015.
 */
public class TimePickerFragment extends DialogFragment {

    public static final String EXTRA_TIME =
            "hcmut.cse.ttcnpm.group13.restaurantbookingonline.booking_time";

    private static final String ARG_TIME = "booking_time";

    // time picker
    private TimePicker mTimePicker;

    private int TIME_PICKER_INTERVAL = 5;
    private int MIN_HOUR = 0;
    private int MAX_HOUR = 23;

    private NumberPicker minutePicker;
    private NumberPicker hourPicker;
    List<String> displayedValues;

    public static TimePickerFragment newInstance(Calendar date) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TIME, date);

        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void sendResult(int resultCode, Calendar date) {
        if (getTargetFragment() == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_TIME, date);

        getTargetFragment()
                .onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = (Calendar) getArguments().getSerializable(ARG_TIME);

        if (calendar == null) {
            calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
        }

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE) / TIME_PICKER_INTERVAL;

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        AlertDialog alertDialog = null;
        if (layoutInflater != null) {
            View v = layoutInflater.inflate(R.layout.dialog_time, null);
            mTimePicker = (TimePicker) v.findViewById(R.id.dialog_time_picker);
            if (mTimePicker != null) {
                mTimePicker.setIs24HourView(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mTimePicker.setMinute(minute);
                    mTimePicker.setHour(hour);
                } else {
                    mTimePicker.setCurrentMinute(minute);
                    mTimePicker.setCurrentHour(hour);
                }

                setTimePickerInterval(mTimePicker);
            }
            alertDialog = new MyDateDialog().createMyAlertDialog(R.string.txt_time_picker_choose_time_title, R.string.txt_time_picker_choose_time_message, R.string.txt_OK, 0);
            alertDialog.setView(v);
        }
        return alertDialog;
    }

    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field minuteField = classForid.getField("minute");
            minutePicker = (NumberPicker) timePicker
                    .findViewById(minuteField.getInt(null));
            minutePicker.setMinValue(0);
            minutePicker.setMaxValue(60 / TIME_PICKER_INTERVAL - 1);

            Field hourField = classForid.getField("hour");
            hourPicker = (NumberPicker) timePicker.findViewById(hourField.getInt(null));
            hourPicker.setMinValue(MIN_HOUR);
            hourPicker.setMaxValue(MAX_HOUR);

            displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }

            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));
            minutePicker.setWrapSelectorWheel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class MyDateDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            int hour;
            int minute;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                hour = mTimePicker.getHour();
                minute = mTimePicker.getMinute() * TIME_PICKER_INTERVAL;
            } else {
                hour = mTimePicker.getCurrentHour();
                minute = mTimePicker.getCurrentMinute() * TIME_PICKER_INTERVAL;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, 2000);
            calendar.set(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, 0);
            sendResult(Activity.RESULT_OK, calendar);
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {
        }

        @Override
        public Activity getCurrentActivity() {
            return TimePickerFragment.this.getActivity();
        }

        @Override
        public AlertDialog createMyAlertDialog(String aTitle, String aMessage, String txtPositiveBtn, String txtNegativeBtn) {
            return super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
        }

        @Override
        public AlertDialog createMyAlertDialog(int aTitle, int aMessage, int txtPositiveBtn, int txtNegativeBtn) {
            return super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
        }
    }
}
