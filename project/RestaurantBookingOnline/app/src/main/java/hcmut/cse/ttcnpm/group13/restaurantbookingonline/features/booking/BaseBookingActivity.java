package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.base.MyFeatureActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment.HomeFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.NetworkUtils;

public class BaseBookingActivity extends MyFeatureActivity {

    // TAG debug
    private static final String PASS_DATA_TAG = "PassTime";

    //RelativeLayout mLayoutMain = null;
    Button previousButton = null;
    Button continueButton = null;
    TextView mTitleActionBookingTextView = null;

    // constant represent state in booking
    private final int CHOOSE_TIME_STATE = 0;
    private final int CHOOSE_PEOPLE_STATE = 1;
    private final int FILL_GUEST_INFO_STATE = 2;
    private final int CHOOSE_REMIND_OPTION_BOOKING_SESSION_STATE = 3;
    private final int CONFIRM_STATE = 4;
    private final int VALIDATION_FINISH_BOOKING_STATE = 5;

    private final int NUMBER_OF_STATE = 6;

    // current step in booking activity
    int currentStep = 0;

    private final int MIN_PEOPLE = 2;
    private final int MAX_PEOPLE = 24;

    // BOOKING ACTION STATE
    static public final int BOOKING_CANCELED = -1;
    static public final int BOOKING_SUCCEED = 1;

    private BookingSessionInfo mBookingSessionInfo;
    private static String mIdSession = null;

    private long mLastClickTime = 0;
    private ProgressDialog mSendingMailProgressDialog = null;

    public static BookingSessionInfo getCurrentSession(Context aContext) {
        BookingSessionInfo curSession = null;
        if (mIdSession != null) {
            curSession = BookingSessionLab.get(aContext).getBookingSession(mIdSession);
        }
        return curSession;
    }

    @Override
    public void setMyFeaturesTitle() {
        setTitle("  " + getString(R.string.txt_booking_title));
    }

    @Override
    public void setMyFeaturesIcon() {
        getSupportActionBar().setIcon(R.drawable.ic_booking_menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base_booking);
        previousButton = (Button) findViewById(R.id.btnPrevious);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                handleClickPreviousButton();
            }
        });

        continueButton = (Button) findViewById(R.id.btnContinue);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (isValidInput(currentStep)) {
                    if (currentStep == CONFIRM_STATE) {
                        AlertDialog confirmDialog = new ConfirmBookingDialog().createMyAlertDialog(R.string.txt_title_confirm_dialog_booking, R.string.txt_message_confirm_dialog_booking,
                                R.string.txt_positive_button_booking, R.string.txt_negative_button_booking);
                        confirmDialog.show();
                    } else {
                        currentStep = (currentStep + 1) % NUMBER_OF_STATE;
                        displayView(currentStep);
                    }
                }
            }
        });

        mTitleActionBookingTextView = (TextView) findViewById(R.id.txtBookingActionTitle);

        mBookingSessionInfo = new BookingSessionInfo();
        mIdSession = mBookingSessionInfo.getId();

        List<BookingSessionInfo> bookingSessionList = BookingSessionLab.get(getApplicationContext()).getBookingSessionList();
        bookingSessionList.add(mBookingSessionInfo);

        if (savedInstanceState == null) {
            displayView(CHOOSE_TIME_STATE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            exitAndReturnToHomeActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (currentStep == 0) {
            exitAndReturnToHomeActivity();
        } else {
            handleClickPreviousButton();
        }
    }

    private void displayView(int step) {
        displayTitle(step);
        Fragment fragment = null;
        switch (step) {
            case CHOOSE_TIME_STATE:
                fragment = new ChooseTimeFragment();
                previousButton.setVisibility(View.GONE);
                break;
            case CHOOSE_PEOPLE_STATE:
                fragment = new ChoosePeopleFragment();
                previousButton.setVisibility(View.VISIBLE);
                break;
            case FILL_GUEST_INFO_STATE:
                fragment = new FillGuestInformationFragment();
                break;
            case CHOOSE_REMIND_OPTION_BOOKING_SESSION_STATE:
                fragment = new ChooseReminderOptionFragment();
                break;
            case CONFIRM_STATE:
                fragment = BookingFullInfoFragment.newInstance(getCurrentSession(getApplicationContext()));
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (transaction != null) {
                transaction.addToBackStack(Integer.toString(currentStep));
                if (step != CHOOSE_TIME_STATE) {
                    transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
                }
                transaction.replace(R.id.frame_container_booking, fragment).commit();
            }
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    private void displayTitle(int step) {
        String titleFragment = "";
        switch (step) {
            case CHOOSE_TIME_STATE:
                titleFragment = getString(R.string.txt_title_choose_time_booking);
                break;
            case CHOOSE_PEOPLE_STATE:
                titleFragment = getString(R.string.txt_title_choose_table_booking);
                break;
            case FILL_GUEST_INFO_STATE:
                titleFragment = getString(R.string.txt_title_personal_info_booking);
                break;
            case CHOOSE_REMIND_OPTION_BOOKING_SESSION_STATE:
                titleFragment = getString(R.string.txt_title_remind_optional_booking);
                break;
            case CONFIRM_STATE:
                titleFragment = "DEAR" + "  " + BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getName();
                break;
            default:
                break;
        }
        mTitleActionBookingTextView.setText(titleFragment);
    }

    private void handleClickPreviousButton() {
        // If not in the first step, back to last step
        if (currentStep > 0) {
            currentStep = (currentStep - 1);
            displayTitle(currentStep);
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager != null) {
                fragmentManager.popBackStackImmediate();
            }
        }

        // If it's transition from step 1 -> step 0, disable previous button
        if (currentStep == 0) {
            previousButton.setVisibility(View.GONE);
        }
    }

    private void exitAndReturnToHomeActivity() {
        AlertDialog alertDialog = new ReturnHomeDialog().createMyAlertDialog(R.string.txt_title_alert_dialog_booking, R.string.txt_message_alert_dialog_booking,
                R.string.txt_positive_button_booking, R.string.txt_negative_button_booking);
        alertDialog.show();
    }


    private class ConfirmBookingDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {

            //boolean isConnect = NetworkUtils.isNetworkConnected(getCurrentActivity());
            new StoreBookingTask().execute();
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {
            dialog.cancel();
        }

        @Override
        public Activity getCurrentActivity() {
            return BaseBookingActivity.this;
        }
    }

    private class NoConnectionDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            dialog.cancel();
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {

        }

        @Override
        public Activity getCurrentActivity() {
            return BaseBookingActivity.this;
        }
    }

    private class StoreBookingTask extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog mBookingProgressDialog = null;

        @Override
        protected void onPreExecute() {
            mBookingProgressDialog = ProgressDialog.show(BaseBookingActivity.this,"BOOKING","Please wait a few seconds...",true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean isConnect = NetworkUtils.isNetworkConnected(getApplicationContext());
            if (isConnect)
            {
                    if (mIdSession != null) {
                        BookingSessionLab.get(getApplicationContext()).removeBookingSession(mIdSession);

                        final ParseObject objectBookingInfor = new ParseObject("BookingInfor");

                        String dateStr = new SimpleDateFormat("dd/MM/yyyy").format(mBookingSessionInfo.getBookingDateOnly().getTime());
                        String timeStr = new SimpleDateFormat("kk:mm").format(mBookingSessionInfo.getBookingTimeOnly().getTime());

                        objectBookingInfor.put("DeviceID", BookingSessionLab.getDeviceId());
                        objectBookingInfor.put("Ten", mBookingSessionInfo.getName());
                        objectBookingInfor.put("email", mBookingSessionInfo.getEmail());
                        objectBookingInfor.put("phone", mBookingSessionInfo.getPhoneNumber());
                        objectBookingInfor.put("Date", dateStr);
                        objectBookingInfor.put("Time", timeStr);
                        objectBookingInfor.put("SoNguoi", mBookingSessionInfo.getNumberOfPeople());
                        objectBookingInfor.put("IsRemind", mBookingSessionInfo.getRemindInfo().getKey());
                        objectBookingInfor.put("RemindInterval", mBookingSessionInfo.getRemindInfo().getValue());
                        objectBookingInfor.put("Note", mBookingSessionInfo.getNote());

                        objectBookingInfor.saveEventually(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                mBookingSessionInfo.setId(objectBookingInfor.getObjectId());
                                HistoryLab.get(getApplicationContext()).addBookedSession(mBookingSessionInfo);

                                String name = mBookingSessionInfo.getName();
                                String receiver = mBookingSessionInfo.getEmail();
                                String validationCode = objectBookingInfor.getString("Active");
                                Calendar date = mBookingSessionInfo.getRealFullBookingDate();
                                new SendValidationCodeTask(receiver, name, validationCode, date).execute();
                            }
                        });
                    }
            }
            return isConnect;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mBookingProgressDialog.dismiss();
            if (result)
            {
                // Finish and call onActivityResult from HomeActivity
                Intent i = new Intent();
                i.putExtra(HomeFragment.BOOKING_STATE, BOOKING_SUCCEED);
                setResult(Activity.RESULT_OK, i);
                BaseBookingActivity.this.finish();
            }
            else {
                AlertDialog noConnectionDialog = new NoConnectionDialog().createMyAlertDialog("BOOKING FAILED","Sorry, no connection available","OK","");
                noConnectionDialog.show();
            }
        }
    }

    private class SendValidationCodeTask extends AsyncTask<Void, Void, Void> {

        private String mName;
        private String mReceiver;
        private String mValidationCode;
        private Calendar mDate;

        public SendValidationCodeTask(String receiver, String name, String validationCode, Calendar date)
        {
            super();
            mReceiver = receiver;
            mName = name;
            mValidationCode = validationCode;
            mDate = date;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String username = getString(R.string.mail_sender_username);
            String password = getString(R.string.mail_sender_password);
            String receiver = mReceiver;
            String title = getString(R.string.mail_title_validation);
            String textMessage = "Thanks " + "<b>" + mName + "</b>" + " for booking in our restaurant <br/><br/>" +
                                 "Your booking will start at <br/>" + "<b>" + new SimpleDateFormat("kk:mm").format(mDate.getTime()) +
                                 "  -  " + new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.ENGLISH).format(mDate.getTime()) + "</b>" +
                                 "<br/><br/>" + "Here\'s your code to validation :" + "<b>" + mValidationCode + "</b>";
            Message codeValidationMessage =
                    NetworkUtils.createGmailMessage(getApplicationContext(),username, password, receiver,title,textMessage);

            try {
                Transport.send(codeValidationMessage);
            } catch (MessagingException e) {
                Toast.makeText(getApplicationContext(),R.string.toast_error_sending_mail, Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
        }
    }

    private class ReturnHomeDialog extends MyUtils.MyAlertDialog {
        @Override
        public Activity getCurrentActivity() {
            return BaseBookingActivity.this;
        }

        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager != null) {
                // Exit and return last activity
                Intent i = new Intent();
                i.putExtra(HomeFragment.BOOKING_STATE, BOOKING_CANCELED);
                setResult(Activity.RESULT_OK, i);
                getCurrentActivity().finish();

                // Remove current booking session and update to database
                if (mIdSession != null) {
                    BookingSessionLab.get(getApplicationContext()).removeBookingSession(mIdSession);
                }
            }
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {
            dialog.cancel();
        }
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */

    /**
     * @param step - index of Action in Booking
     * @return true - if no error, and false if invalid
     */
    private boolean isValidInput(int step) {
        switch (step) {
            case CHOOSE_TIME_STATE:
                if (!isValidDateAndTimeInput()) return false;
                break;
            case CHOOSE_PEOPLE_STATE:
                if (!isValidPeopleInput()) return false;
                break;
            case FILL_GUEST_INFO_STATE:
                if (!isValidGuestInfoInput()) return false;
                break;
            case CHOOSE_REMIND_OPTION_BOOKING_SESSION_STATE:
                break;
            case CONFIRM_STATE:
                break;
            default:
                break;
        }
        return true;
    }

    private boolean isValidDateAndTimeInput() {
        Calendar date = BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getBookingDateOnly();
        Calendar time = BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getBookingTimeOnly();
        if (date == null || time == null) {
            Toast dateAndTimeToast = Toast.makeText(getApplicationContext(), R.string.toast_miss_date_or_time, Toast.LENGTH_SHORT);
            dateAndTimeToast.show();
            return false;
        }
        return true;
    }

    private boolean isValidPeopleInput() {
        int numberOfPeople = BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getNumberOfPeople();
        if (numberOfPeople < MIN_PEOPLE) {
            Toast minPeopleToast = Toast.makeText(getApplicationContext(), getString(R.string.toast_min_people_num) + " " + Integer.toString(MIN_PEOPLE), Toast.LENGTH_SHORT);
            minPeopleToast.show();
            return false;
        } else if (numberOfPeople > MAX_PEOPLE) {
            Toast maxPeopleToast = Toast.makeText(getApplicationContext(), getString(R.string.toast_max_people_num) + " " + Integer.toString(MAX_PEOPLE), Toast.LENGTH_SHORT);
            maxPeopleToast.show();
            return false;
        }
        return true;
    }

    private boolean isValidGuestInfoInput() {
        String name = BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getName();
        // Name field must be filled
        if (name.equals("")) {
            Toast nameToast = Toast.makeText(getApplicationContext(), R.string.toast_miss_guest_name, Toast.LENGTH_SHORT);
            nameToast.setMargin(0, -0.05f);
            nameToast.show();
            return false;
        }

        // Email checking
        else {
            String email = BookingSessionLab.get(getApplicationContext()).getBookingSession(mIdSession).getEmail();
            final Pattern emailPattern = Pattern.compile("([a-zA-Z0-9_.+-]+)(@)([a-zA-Z0-9-]+)(\\.[a-zA-Z0-9-]+)*");
            final Matcher emailEditTextMatcher = emailPattern.matcher(email);
            /// If email didn't right format and your input not empty

            if (email.equals("")) {
                Toast requiredEmailToast = Toast.makeText(getApplicationContext(), R.string.toast_miss_email, Toast.LENGTH_SHORT);
                requiredEmailToast.setMargin(0, -0.05f);
                requiredEmailToast.show();
                return false;
            } else if (emailEditTextMatcher.matches() == false) {
                Toast emailToast = Toast.makeText(getApplicationContext(), R.string.toast_invalid_email_format, Toast.LENGTH_SHORT);
                emailToast.setMargin(0, -0.05f);
                emailToast.show();
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
