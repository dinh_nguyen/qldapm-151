package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.content.Context;
import android.provider.Settings;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
//import java.util.UUID;

/**
 * Created by dinh on 11/17/2015.
 */
public class BookingSessionLab {

    private static BookingSessionLab sBookingSessionLab;

    private static List<BookingSessionInfo> mBookingSessionList;

    private static String androidId = "";

    private BookingSessionLab(Context aContext) {
        mBookingSessionList = new ArrayList<>();
        androidId = Settings.Secure.getString(aContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static BookingSessionLab get(Context context) {
        if (sBookingSessionLab == null) {
            sBookingSessionLab = new BookingSessionLab(context);
        }
        return sBookingSessionLab;
    }

    public static List<BookingSessionInfo> getBookingSessionList() {
        return mBookingSessionList;
    }

    public static String getDeviceId() {
        return androidId;
    }

    public BookingSessionInfo getBookingSession(String id) {
        for (BookingSessionInfo session : mBookingSessionList) {
            if (session.getId().equals(id)) {
                return session;
            }
        }
        return null;
    }

    public boolean removeBookingSession(String id) {
        BookingSessionInfo session = getBookingSession(id);
        if (session != null) {
            mBookingSessionList.remove(session);
            return true;
        }
        return false;
    }

    public int getCounterSessionVerify() {
        int count = 0;
        for (BookingSessionInfo booking : mBookingSessionList) {
            if (booking.getState() == BookingSessionInfo.IN_COMING) count += 1;
        }
        return count;
    }

    public int getCounterSessionNotVerfy() {
        int count = 0;
        for (BookingSessionInfo booking : mBookingSessionList) {
            if (booking.getState() == BookingSessionInfo.INVALIDATE) count += 1;
        }
        return count;
    }
}
