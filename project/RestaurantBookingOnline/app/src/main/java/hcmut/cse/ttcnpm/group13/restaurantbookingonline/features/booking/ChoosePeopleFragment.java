package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.UI.CustomEditText;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChoosePeopleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChoosePeopleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChoosePeopleFragment extends Fragment {
    public ChoosePeopleFragment() {
    }

    private CustomEditText mEditTextPeopleNum;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choose_people, container, false);

        if (rootView != null) {
            mEditTextPeopleNum = (CustomEditText) rootView.findViewById(R.id.editTxtPeopleNum);
            if (mEditTextPeopleNum != null) {
                int numPeople = BaseBookingActivity.getCurrentSession(getActivity()).getNumberOfPeople();
                mEditTextPeopleNum.setText(Integer.toString(numPeople));
//                mEditTextPeopleNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        if (actionId == EditorInfo.IME_ACTION_DONE) {
////                            mEditTextPeopleNum.setSelectAllOnFocus(false);
////                            mEditTextPeopleNum.setCursorVisible(false);
////                            mEditTextPeopleNum.clearFocus();
//                        }
//                        return false;
//                    }
//
//                });


                mEditTextPeopleNum.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String str = s.toString();
                        int n = 0;
                        if (!str.equals("")) {
                            try {
                                n = Integer.parseInt(str);
                            } catch (NumberFormatException nfe) {
                                Toast.makeText(getActivity().getApplicationContext(), R.string.toast_invalid_number, Toast.LENGTH_SHORT).show();
                            }
                        }
                        //BaseBookingActivity.mNumberOfPeople = n;
                        BaseBookingActivity.getCurrentSession(getActivity()).setNumberOfPeople(n);
                    }
                });
            }
        }
        return rootView;
    }
}
