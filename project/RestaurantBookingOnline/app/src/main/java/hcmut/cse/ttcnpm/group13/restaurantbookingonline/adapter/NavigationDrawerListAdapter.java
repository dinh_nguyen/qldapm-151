package hcmut.cse.ttcnpm.group13.restaurantbookingonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.model.NavigationDrawerItem;

/**
 * Created by dinh on 11/6/2015.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    private Context mContext = null;
    private ArrayList<NavigationDrawerItem> mNavigationDrawerItems;

    public NavigationDrawerListAdapter(Context context, ArrayList<NavigationDrawerItem> navigationDrawerItems) {
        mContext = context;
        mNavigationDrawerItems = navigationDrawerItems;
    }

    @Override
    public int getCount() {
        return mNavigationDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mNavigationDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.drawer_list_item, null);
        }

        if (convertView != null) {
            final NavigationDrawerItem item = mNavigationDrawerItems.get(position);

            viewHolder = new ViewHolder();

            viewHolder.imageIcon = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.textTitle = (TextView) convertView.findViewById(R.id.title);
            viewHolder.textCount = (TextView) convertView.findViewById(R.id.counter);

            if (viewHolder.imageIcon != null)
                viewHolder.imageIcon.setImageResource(item.getIcon());
            if (viewHolder.textTitle != null)
                viewHolder.textTitle.setText(item.getTitle());

            if (item.isCounterVisible()) {
                viewHolder.textCount.setText(Integer.toString(item.getCount()) + "+");
                viewHolder.textCount.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textCount.setVisibility(View.GONE);
            }
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView imageIcon;
        TextView textTitle;
        TextView textCount;
    }
}
