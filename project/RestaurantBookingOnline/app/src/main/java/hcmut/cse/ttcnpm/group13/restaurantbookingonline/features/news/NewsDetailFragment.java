package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Locale;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailFragment extends Fragment {


    public NewsDetailFragment() {
        // Required empty public constructor
    }

    // Current news in fragment
    private News mNews = null;

    // Current fields in fragment
    private ImageView mHeaderImageView = null;
    private TextView mTitleTextView = null;
    private TextView mContentTextView = null;
    private ImageView mIconAuthorImageView = null;
    private TextView mAuthorTextView = null;
    private TextView mDateNewsTextView = null;

    // Facebook share button
    private ShareButton mFacebookShareButton = null;

    private static final String ARG_NEWS_ID = "news_id";

    public static NewsDetailFragment newInstance(String newsId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_NEWS_ID, newsId);

        NewsDetailFragment fragment = new NewsDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            String newsId = (String) args.getSerializable(ARG_NEWS_ID);
            mNews = NewsLab.get(getActivity()).getNews(newsId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_news_detail, container, false);

        if (rootView != null) {
            mHeaderImageView = (ImageView) rootView.findViewById(R.id.newsDetailImageView);
            if (mHeaderImageView != null) {
                Picasso.with(getContext())
                        .load(mNews.getIconId())
                        .placeholder(R.drawable.ic_place_holder)
                        .error(R.drawable.ic_place_holder)
                        .into(mHeaderImageView);
            }

            mTitleTextView = (TextView) rootView.findViewById(R.id.newsDetailTitleTextView);
            if (mTitleTextView != null) {
                mTitleTextView.setText(mNews.getTitle());
            }

            mIconAuthorImageView = (ImageView) rootView.findViewById(R.id.newsDetailLogoImageView);
            if (mIconAuthorImageView != null) {
                mIconAuthorImageView.setImageResource(mNews.getAuthorId());
            }

            mAuthorTextView = (TextView) rootView.findViewById(R.id.newsDetailAuthorTextView);
            if (mAuthorTextView != null) {
                mAuthorTextView.setText(mNews.getAuthor());
            }

            mDateNewsTextView = (TextView) rootView.findViewById(R.id.newsDetailDateTextView);
            if (mDateNewsTextView != null) {
                mDateNewsTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy | kk:mm", Locale.ENGLISH)
                        .format(mNews.getDate().getTime()));
            }

            mContentTextView = (TextView) rootView.findViewById(R.id.newsDetailContentTextView);
            if (mContentTextView != null) {
                mContentTextView.setText(mNews.getContent());
            }

            mFacebookShareButton = (ShareButton)rootView.findViewById(R.id.btnFacebookShareNews);
            if (mFacebookShareButton != null)
            {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentTitle(mNews.getTitle())
                        .setContentDescription(mNews.getDescription())
                        .setContentUrl(Uri.parse(mNews.getNewsLink()))
                        .setImageUrl(Uri.parse(mNews.getIconId()))
                        .build();

                mFacebookShareButton.setShareContent(content);
            }

        }
        return rootView;
    }
}
