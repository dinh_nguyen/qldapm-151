package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.HomeActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BaseBookingActivity;

public class HomeFragment extends Fragment {

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    // button booking
    private Button mButtonBooking = null;

    // Facebook share
    private ShareButton mFacebookShareButton;

    // state
    static public final String BOOKING_STATE = "booking_state";

    static public final int REQUEST_CODE_FROM_HOME_FRAGMENT = 101;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mButtonBooking = (Button) rootView.findViewById(R.id.btnBookingTable);
        if (mButtonBooking != null) {
            mButtonBooking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Prevent user pressed many times
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    ////////////////////////////////////////////////////////////////
                    Intent i = new Intent(getActivity(), BaseBookingActivity.class);
                    getActivity().startActivityForResult(i, REQUEST_CODE_FROM_HOME_FRAGMENT);
                }
            });
        }

        mFacebookShareButton = (ShareButton)rootView.findViewById(R.id.btnFacebookShare);

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle("NHA HANG IZZY")
                .setContentDescription("Chao mung vieng tham trang chu nha hang Izzy...")
                .setContentUrl(Uri.parse("https://izzybooking2015.wordpress.com/nha-hang-izzy/"))
                .build();

        mFacebookShareButton.setShareContent(content);

        return rootView;
    }

//    public static Bitmap loadBitmapFromView(View v) {
//        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Canvas c = new Canvas(b);
//        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
//        v.draw(c);
//        return b;
//    }
}
