package hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by dinh on 11/10/2015.
 */


public class MyUtils {

    static public abstract class MyAlertDialog {

        public abstract void onClickPositiveButton(DialogInterface dialog, int id);

        public abstract void onClickNegativeButton(DialogInterface dialog, int id);

        public abstract Activity getCurrentActivity();

        public AlertDialog createMyAlertDialog(String aTitle, String aMessage, String txtPositiveBtn, String txtNegativeBtn) {
            // Customize Alert Dialog
            Activity activity = getCurrentActivity();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

            alertDialogBuilder.setTitle(aTitle);
            alertDialogBuilder.setMessage(aMessage);
            alertDialogBuilder.setCancelable(false);

            if (txtPositiveBtn != "") {
                alertDialogBuilder.setPositiveButton(txtPositiveBtn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onClickPositiveButton(dialog, id);
                    }
                });
            }

            if (txtNegativeBtn != "") {
                alertDialogBuilder.setNegativeButton(txtNegativeBtn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onClickNegativeButton(dialog, id);
                    }
                });
            }

            // Create and display customized alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            return alertDialog;
        }

        public AlertDialog createMyAlertDialog(int aTitle, int aMessage, int txtPositiveBtn, int txtNegativeBtn) {
            Activity activity = getCurrentActivity();
            Resources resources = activity.getResources();
            AlertDialog alertDialog = null;
            if (resources != null) {
                String stringTitle = (aTitle <= 0) ? "" : resources.getString(aTitle);
                String stringMessage = (aMessage <= 0) ? "" : resources.getString(aMessage);
                String stringPositiveBtn = (txtPositiveBtn <= 0) ? "" : resources.getString(txtPositiveBtn);
                String stringNegativeBtn = (txtNegativeBtn <= 0) ? "" : resources.getString(txtNegativeBtn);
                alertDialog = createMyAlertDialog(stringTitle, stringMessage, stringPositiveBtn, stringNegativeBtn);
            }
            return alertDialog;
        }
    }

    public static class MyPair implements Serializable {
        boolean mKey = false;
        int mValue = 0;

        public MyPair(boolean aKey, int aValue) {
            mKey = aKey;
            mValue = aValue;
        }

        public MyPair() {
            mKey = false;
            mValue = 0;
        }

        boolean compareKey(MyPair otherPair) {
            return this.mKey == otherPair.mKey;
        }

        boolean compareValue(MyPair otherPair) {
            return this.mValue == otherPair.mValue;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MyPair myPair = (MyPair) o;

            if (mKey != myPair.mKey) return false;
            return mValue == myPair.mValue;
        }

        @Override
        public int hashCode() {
            int result = (mKey ? 1 : 0);
            result = 31 * result + mValue;
            return result;
        }

        public boolean getKey() {
            return mKey;
        }

        public void setKey(boolean key) {
            mKey = key;
        }

        public int getValue() {
            return mValue;
        }

        public void setValue(int value) {
            mValue = value;
        }
    }

    // Add red * character to the end of string
    static public SpannableStringBuilder getStringAfterAddSubfix(String originString, String coloredString, int aColor) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(originString);
        int start = builder.length();
        builder.append(coloredString);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(aColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder;
    }

    static public StringBuilder getStringAfterCompareToday(Calendar calendar) {
        StringBuilder tmp = new StringBuilder();
        Calendar todayCalendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int todayYear = todayCalendar.get(Calendar.YEAR);
        int deltaYear = todayYear - year;
        if (deltaYear > 0) {
            if (deltaYear == 1) {
                tmp.append("A year ago");
            } else {
                tmp.append(deltaYear + " " + "years ago");
            }
        } else {
            int month = calendar.get(Calendar.MONTH);
            int todayMonth = todayCalendar.get(Calendar.MONTH);
            int deltaMonth = todayMonth - month;
            if (deltaMonth > 0) {
                if (deltaMonth == 1) {
                    tmp.append("A month ago");
                } else {
                    tmp.append(deltaMonth + " " + "months ago");
                }
            } else {
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int today = todayCalendar.get(Calendar.DAY_OF_MONTH);
                int deltaDay = today - day;
                if (deltaDay > 0) {
                    if (deltaDay == 1) {
                        tmp.append("A day ago");
                    } else {
                        tmp.append(deltaDay + " " + "days ago");
                    }
                } else {
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int todayHour = todayCalendar.get(Calendar.HOUR_OF_DAY);
                    int deltaHour = todayHour - hour;
                    if (deltaHour > 0) {
                        if (deltaHour == 1) {
                            tmp.append("An hour ago");
                        } else {
                            tmp.append(deltaHour + " " + "hours ago");
                        }
                    } else {
                        int minute = calendar.get(Calendar.MINUTE);
                        int todayMinute = todayCalendar.get(Calendar.MINUTE);
                        int deltaMinute = todayMinute - minute;
                        if (deltaMinute >= 0) {
                            if (deltaMinute <= 1) {
                                tmp.append("A minute ago");
                            } else {
                                tmp.append(deltaMinute + " " + "minutes ago");
                            }
                        }
                    }
                }
            }
        }
        return tmp;
    }

    static public String getStringFromReminderTime(int time) {
        String str = "";
        if (time < 60) {
            str = "minute";
        } else if (time < 1440) {
            time = time / 60;
            str = "hour";
        } else {
            time = time / 1440;
            str = "day";
        }
        if (time > 1) str = str + "s";
        str = time + " " + str;
        return str;
    }

    static public StringBuilder getStringAfterCompare(Calendar startCalendar, Calendar endCalendar) {
        StringBuilder tmp = new StringBuilder("");

        long endDateInMillis = endCalendar.getTimeInMillis();
        long startDateInMillis = startCalendar.getTimeInMillis();

        tmp.append(DateUtils.getRelativeTimeSpanString(startDateInMillis, endDateInMillis, 0).toString());
        return tmp;
    }

    static public StringBuilder getStringAfterCompareExpiredDate(Calendar expiredCalendar, Calendar todayCalendar) {
        StringBuilder tmp = new StringBuilder("");
        long expiredDateInMillis = expiredCalendar.getTimeInMillis();
        long todayInMillis = todayCalendar.getTimeInMillis();

        if (todayInMillis - expiredDateInMillis >= 0) {
            tmp.append("Expired");
        } else {
            CharSequence compareDateStr = DateUtils.getRelativeTimeSpanString(expiredDateInMillis, todayInMillis, 0);
            tmp.append(compareDateStr);
        }
        return tmp;
    }

    public static final long MILLISECOND_PER_DAY = 24 * 60 * 60 * 1000;

    public static int getSignedDiffDaysBetween(Calendar beginCalendar, Calendar endCalendar) {
        long diff = (endCalendar.getTimeInMillis() - beginCalendar.getTimeInMillis()) / MILLISECOND_PER_DAY;
        return (int) diff;
    }

    public static int getUnsignedDiffDaysBetween(Calendar beginCalendar, Calendar endCalendar) {
        long diff = (endCalendar.getTimeInMillis() - beginCalendar.getTimeInMillis()) / MILLISECOND_PER_DAY;
        return (int) Math.abs(diff);
    }

}
