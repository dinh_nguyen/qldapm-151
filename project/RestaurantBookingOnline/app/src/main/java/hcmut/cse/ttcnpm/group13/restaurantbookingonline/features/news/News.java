package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news;

import java.util.Calendar;
import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * Created by dinh on 11/13/2015.
 */
public class News {
    private String mId;
    // image header and icon id
    private String mIconId;
    // image represent author
    private int mAuthorId = R.drawable.ic_restaurant_splash_screen;
    // link to this news
    private String mNewsLink;
    // author write this news
    private String mAuthor;
    private String mTitle;
    private String mContent;
    private String mDescription;
    private Calendar mDate;
    private boolean mIsNew;

    public News() {
    }

    public News(String aId ,String aIconId, String aNewsLink, String aAuthor, String aTitle, String aContent, String aDescription, Calendar aDate, boolean aIsNew) {
        mId = aId;
        mIconId = aIconId;
        mNewsLink = aNewsLink;
        mAuthor = aAuthor;
        mTitle = aTitle;
        mContent = aContent;
        mDescription = aDescription;
        mDate = aDate;
        mIsNew = aIsNew;
    }

    public String getNewsLink() {
        return mNewsLink;
    }

    public void setNewsLink(String newsLink) {
        mNewsLink = newsLink;
    }

    public String getId() {
        return mId;
    }

    public String getIconId() {
        return mIconId;
    }

    public void setIconId(String aIconId) {
        mIconId = aIconId;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Calendar getDate() {
        return mDate;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setDate(Calendar date) {
        mDate = date;
    }

    public boolean isNew() {
        return mIsNew;
    }

    public void setIsNew(boolean isNew) {
        this.mIsNew = isNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (mAuthorId != news.mAuthorId) return false;
        if (mId != null ? !mId.equals(news.mId) : news.mId != null) return false;
        if (mIconId != null ? !mIconId.equals(news.mIconId) : news.mIconId != null) return false;
        if (mAuthor != null ? !mAuthor.equals(news.mAuthor) : news.mAuthor != null) return false;
        if (mTitle != null ? !mTitle.equals(news.mTitle) : news.mTitle != null) return false;
        if (mContent != null ? !mContent.equals(news.mContent) : news.mContent != null)
            return false;
        if (mDescription != null ? !mDescription.equals(news.mDescription) : news.mDescription != null)
            return false;
        return !(mDate != null ? !mDate.equals(news.mDate) : news.mDate != null);

    }

    @Override
    public int hashCode() {
        int result = mId != null ? mId.hashCode() : 0;
        result = 31 * result + (mIconId != null ? mIconId.hashCode() : 0);
        result = 31 * result + mAuthorId;
        result = 31 * result + (mAuthor != null ? mAuthor.hashCode() : 0);
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        result = 31 * result + (mContent != null ? mContent.hashCode() : 0);
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        result = 31 * result + (mDate != null ? mDate.hashCode() : 0);
        return result;
    }
}
