package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.util.ArrayList;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/21/2015.
 */
public class NotifyService extends Service {

    // Name of an intent we can use to notify if this service was started to create a notification
    public static final String INTENT_NOTIFY = "hcmut.cse.ttcnpm.group13.reminderdemo.INTENT_NOTIFY";
    // Name of an intent we can use to notify if this service was started to create a notification in boot
    public static final String INTENT_NOTIFY_BOOT = "hcmut.cse.ttcnpm.group13.reminderdemo.INTENT_NOTIFY_BOOT";
    // Name of unique id parameter we pass on
    public static final String ARG_NOTIFICATION_ID = "hcmut.cse.ttcnpm.group13.reminderdemo.NOTIFICATION_ID";
    // Name of unique id parameter we pass on
    public static final String ARG_NOTIFICATION_LIST = "hcmut.cse.ttcnpm.group13.reminderdemo.NOTIFICATION_LIST";
    // System notification manager
    private NotificationManager mNotificationManager;
    // Object receives interactions from clients
    private final IBinder mBinder = new ServiceBinder();

    public static Intent newIntent(Context context, int id) {
        Intent intent = new Intent(context, NotifyService.class);
        intent.putExtra(INTENT_NOTIFY, true);
        intent.putExtra(ARG_NOTIFICATION_ID, id);
        return intent;
    }

    public static Intent newIntentBoot(Context context, ArrayList<Object> listSession, int id) {
        Intent intent = new Intent(context, NotifyService.class);
        intent.putExtra(INTENT_NOTIFY_BOOT, true);
        intent.putExtra(ARG_NOTIFICATION_ID, id);
        intent.putExtra(ARG_NOTIFICATION_LIST, listSession);
        return intent;
    }

    @Override
    public void onCreate() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public int onStartCommand(Intent intent, int flags, int startId) {
        BookingSessionInfo session = null;
        int id = intent.getIntExtra(ARG_NOTIFICATION_ID, -1);
        if (intent.getBooleanExtra(INTENT_NOTIFY, false)) {
            if (id != -1) {
                session = ReminderLab.get(this).getReminderList().get(id);
                showNotification(session, id);
            }
        } else if (intent.getBooleanExtra(INTENT_NOTIFY_BOOT, false)) {
            ArrayList<Object> listSession = (ArrayList<Object>) intent.getSerializableExtra(ARG_NOTIFICATION_LIST);
            if (listSession != null) {
                if (id != -1) {
                    session = (BookingSessionInfo) listSession.get(id);
                    if (session != null) {
                        showNotification(session, id);
                    }
                }
            }
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void showNotification(BookingSessionInfo session, int uniqueId) {

        CharSequence title = session.getName();
        CharSequence text = "Your booking will come in" + " " + MyUtils.getStringFromReminderTime(session.getRemindInfo().getValue());
        long time = System.currentTimeMillis();

        Intent intent = AnnouncementActivity.newIntent(this, session);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // add the back stack
        stackBuilder.addParentStack(AnnouncementActivity.class);
        stackBuilder.addNextIntent(intent);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = stackBuilder.getPendingIntent(uniqueId, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification;
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long vibratePattern[] = {0, 50, 100, 50, 100, 50, 100, 400, 100, 300, 100, 350, 50, 200, 100, 100, 50, 600};

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(title)
                .setContentText(text)
                .setTicker("You have booking coming")
                .setWhen(time)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        builder.setSmallIcon(R.drawable.ic_notification_small);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_icon));
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setVibrate(vibratePattern);
        builder.setLights(Color.BLUE, 500, 500);
        builder.setSound(alarmSound);

        notification = builder.build();

        // Send the notification to the system
        mNotificationManager.notify(uniqueId, notification);

        // Stop the service when we are finished
        stopSelf();
    }

    public class ServiceBinder extends Binder {
        NotifyService getService() {
            return NotifyService.this;
        }
    }
}
