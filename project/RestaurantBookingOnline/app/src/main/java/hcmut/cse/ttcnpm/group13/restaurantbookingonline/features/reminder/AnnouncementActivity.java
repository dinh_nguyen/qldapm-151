package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.parse.Parse;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.base.MyFeatureActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingFullInfoFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsLab;

public class AnnouncementActivity extends MyFeatureActivity {

    private static final String ARG_ANNOUNCEMENT_BOOKING_SESSION = "hcmut.cse.ttcnpm.group13.reminderdemo.ANNOUNCEMENT_BOOKING_SESSION";

    BookingSessionInfo bookingSession = null;

    TextView mTitleTextView = null;

    public static Intent newIntent(Context aContext, BookingSessionInfo session) {
        Intent intent = new Intent(aContext, AnnouncementActivity.class);
        intent.putExtra(ARG_ANNOUNCEMENT_BOOKING_SESSION, session);
        return intent;
    }

    @Override
    public void setMyFeaturesTitle() {
        setTitle("  " + getString(R.string.txt_booking_information_title));
    }

    @Override
    public void setMyFeaturesIcon() {
        getSupportActionBar().setIcon(R.drawable.ic_booking_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);

        mTitleTextView = (TextView) findViewById(R.id.txtAnnouncementTitleTextView);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bookingSession = (BookingSessionInfo) bundle.getSerializable(ARG_ANNOUNCEMENT_BOOKING_SESSION);
            mTitleTextView.setText("DEAR" + "  " + bookingSession.getName());
        }

        if (savedInstanceState == null) {
            displayView();
        }
    }

    private void displayView() {
        BookingFullInfoFragment fragment = BookingFullInfoFragment.newInstance(bookingSession);
        fragment.setVisibleFooterTextView(false);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (transaction != null) {
            transaction.replace(R.id.frame_container_announcement, fragment).commit();
        }
    }

}
