package hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * Created by dinh on 11/22/2015.
 */
public class NetworkUtils {

    public static boolean isNetworkConnected(Context aContext) {
        // This implementation don't run in some emulator when ICMP was blocked
//        try {
//            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1    www.google.com");
//            int returnVal = p1.waitFor();
//            return (returnVal == 0);
//        } catch (Exception e) {
//            return false;
//        }

        // This implementation must be run in background
        if (isNetworkAvailable(aContext))
        {
            if (connectGoogle()) {
                return true;
            }
            else {
                return connectGoogle();
            }
        }
        else
        {
            return false;
        }
    }

    private static boolean isNetworkAvailable(Context   ct)
    {
        ConnectivityManager   connectivityManager = (ConnectivityManager)ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private static boolean connectGoogle() {
        try {
            HttpURLConnection urlc = (HttpURLConnection)(new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(2000);
            urlc.connect();
            return (urlc.getResponseCode() == 200);

        } catch (IOException e) {
            return false;
        }
    }

    public static Message createGmailMessage(Context aContext ,final String userName, final String password, final String mailDestination, String subject, String textMessage) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        });

        Message message = null;
        try
        {
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(userName));
            message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(mailDestination));
            message.setSubject(subject);
            message.setContent(textMessage, "text/html; charset=utf-8");
        }
        catch (MessagingException e)
        {
            Toast.makeText(aContext, R.string.toast_error_create_mail,Toast.LENGTH_SHORT).show();
        }

        return message;
    }
}