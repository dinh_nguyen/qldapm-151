package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.base.MyFeatureActivity;

public class PromotionsPagerActivity extends MyFeatureActivity {

    private static final String EXTRA_PROMOTION_ID =
            "hcmut.cse.ttcnpm.group13.restaurantbookingonline.promotion_id";

    private ViewPager mPromotionViewPager;
    private List<Promotion> mPromotionList;

    private HashMap<String, Integer> mapPromotionListUUID;

    @Override
    public void setMyFeaturesTitle() {
        setTitle("  " + getString(R.string.txt_promotion_detail_title));
    }

    @Override
    public void setMyFeaturesIcon() {
        getSupportActionBar().setIcon(R.drawable.ic_promotion_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions_pager);

        mPromotionList = PromotionsLab.get(this).getPromotionsList();
        mapPromotionListUUID = new HashMap<String, Integer>();
        // Map News UUID to Id
        for (int i = 0; i < mPromotionList.size(); i++) {
            mapPromotionListUUID.put(mPromotionList.get(i).getId(), i);
        }

        mPromotionViewPager = (ViewPager) findViewById(R.id.activity_promotions_pager);
        if (mPromotionViewPager != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            mPromotionViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                @Override
                public Fragment getItem(int position) {
                    Promotion promotion = mPromotionList.get(position);
                    return PromotionsDetailFragment.newInstance(promotion.getId());
                }

                @Override
                public int getCount() {
                    return mPromotionList.size();
                }
            });

            mPromotionViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    Promotion promotion = mPromotionList.get(position);
                    if (promotion != null) {
                        promotion.setIsNew(false);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            Intent curIntent = getIntent();
            if (curIntent != null) {
                String promotionId = (String) curIntent.getSerializableExtra(EXTRA_PROMOTION_ID);
                int currentPosition = mapPromotionListUUID.get(promotionId);
                Promotion promotion = mPromotionList.get(currentPosition);
                if (promotion != null) {
                    promotion.setIsNew(false);
                }
                mPromotionViewPager.setCurrentItem(currentPosition);
            }
        }
    }

    public static Intent newIntent(Context packageContext, String promotionId) {
        Intent intent = new Intent(packageContext, PromotionsPagerActivity.class);
        intent.putExtra(EXTRA_PROMOTION_ID, promotionId);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
