package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryPagerActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;

/**
 * Created by dinh on 11/6/2015.
 */
public class HistoryListFragment extends Fragment {

    public HistoryListFragment() {
    }

    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    private RecyclerView mHistoryRecyclerView;
    private HistoryAdapter mHistoryAdapter;

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.booking_menuRefresh:
                Log.d("Menu", "onOptionItemSelected In HistoryFragment");
                new UpdateHistoryListTask().execute();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_history_list, container, false);

        mHistoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.history_recycler_view);
        if (mHistoryRecyclerView != null) {
            mHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            updateData();
            updateUI();
        }

        return rootView;
    }

    private void updateData() {
        HistoryLab historyLab = HistoryLab.get(getActivity());
        historyLab.updateBookingState();
    }

    private void updateUI() {
        if (mHistoryAdapter == null) {
            HistoryLab historyLab = HistoryLab.get(getActivity());
            List<BookingSessionInfo> bookedList = historyLab.getBookedList();
            mHistoryAdapter = new HistoryAdapter(bookedList);
            mHistoryRecyclerView.setAdapter(mHistoryAdapter);
        }
        else
        {
            HistoryLab historyLab = HistoryLab.get(getActivity());
            List<BookingSessionInfo> bookedList = historyLab.getBookedList();
            mHistoryAdapter.notifyDataSetChanged();
            mHistoryAdapter.notifyItemRangeChanged(0,bookedList.size());
        }
    }

    private class UpdateHistoryListTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            while(HistoryLab.isLoadingData());
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Log.d("Menu", "Adapter In HistoryFragment Updated");
            updateUI();
        }
    }

    private class HistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private BookingSessionInfo mBookingSessionInfo;

        private TextView mDateTextView = null;
        private TextView mTimeTextView = null;
        private TextView mEmailTextView = null;
        private TextView mStatusTextView = null;

        private View mBackground;

        public View getBackground() {
            return mBackground;
        }

        public void setBackground(View background) {
            mBackground = background;
        }

        public HistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mBackground = itemView;

            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_history_date_text_view);
            mTimeTextView = (TextView) itemView.findViewById(R.id.list_item_history_time_text_view);
            mEmailTextView = (TextView) itemView.findViewById(R.id.list_item_history_email_text_view);
            mStatusTextView = (TextView) itemView.findViewById(R.id.list_item_history_status_text_view);
        }

        public void bindHistory(BookingSessionInfo bookingSessionInfo) {

            mBookingSessionInfo = bookingSessionInfo;

            Calendar date = bookingSessionInfo.getBookingDateOnly();
            Calendar time = bookingSessionInfo.getBookingTimeOnly();

            if (date != null) {
                mDateTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy").format(date.getTime()));
            }

            if (time != null) {
                mTimeTextView.setText(new SimpleDateFormat("kk:mm").format(time.getTime()));
                // Set time to current date
                date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
                date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
                date.set(Calendar.SECOND, time.get(Calendar.SECOND));
            }

            mEmailTextView.setText(bookingSessionInfo.getEmail());

            int bookingState = mBookingSessionInfo.getState();
            switch (bookingState) {
                case BookingSessionInfo.EXPIRED:
                    mStatusTextView.setText("EXPIRED");
                    break;
                case BookingSessionInfo.INVALIDATE:
                    mStatusTextView.setText("INVALIDATE");
                    break;
                case BookingSessionInfo.IN_COMING:
                    mStatusTextView.setText("IN COMING");
                    break;
            }
        }

        @Override
        public void onClick(View v) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            // highlight selected item
            int pos = getAdapterPosition();

            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
                mBackground.setSelected(false);
            } else {
                selectedItems.put(pos, true);
                mBackground.setSelected(true);
            }

            // Set new label invisible when clicked
            Intent intent = HistoryPagerActivity.newIntent(getActivity(), mBookingSessionInfo.getId());
            startActivity(intent);
        }
    }

    private class HistoryAdapter extends RecyclerView.Adapter<HistoryHolder> {
        private List<BookingSessionInfo> mBookedList;

        public HistoryAdapter(List<BookingSessionInfo> bookedList) {
            mBookedList = bookedList;
        }

        @Override
        public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_history, parent, false);
            HistoryHolder holder = new HistoryHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(HistoryHolder holder, int position) {
            BookingSessionInfo bookingSession = mBookedList.get(position);
            holder.bindHistory(bookingSession);
        }

        @Override
        public int getItemCount() {
            return mBookedList.size();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
}
