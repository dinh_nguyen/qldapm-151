package hcmut.cse.ttcnpm.group13.restaurantbookingonline.base;

import android.app.Application;
import android.content.res.Configuration;

import com.facebook.FacebookSdk;
import com.parse.Parse;

import java.util.Locale;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.LocaleUtils;

/**
 * Created by dinh on 11/15/2015.
 */
public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        LocaleUtils.setLocale(Locale.ENGLISH);
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());

        // Enable Local Data store.
        Parse.enableLocalDatastore(this);
        //initialization code
        Parse.initialize(this, "v2NMCqnJhuaXUHK7XiRQq4PzOlJVnNmp8bp6jLBl", "3qfhwWDM0j8xLaVBSsctYZ4nVAYtTeHbD50GPvQf");

        // Enable facebook API
        FacebookSdk.sdkInitialize(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }
}
