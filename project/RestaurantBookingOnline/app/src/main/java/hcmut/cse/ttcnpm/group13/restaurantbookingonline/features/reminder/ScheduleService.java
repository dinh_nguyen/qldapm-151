package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by dinh on 11/21/2015.
 */
public class ScheduleService extends Service {

    private final IBinder mBinder = new ServiceBinder();

    public class ServiceBinder extends Binder {
        ScheduleService getService() {
            return ScheduleService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void setAlarm(int index) {
        // This starts a new thread to set the alarm
        // You want to push off your tasks onto a new thread to free up the UI to carry on responding
        new AlarmTask(this, index).run();
    }

    public void setAlarmBoot(Context context, ArrayList<Object> listBookingSession) {
        new AlarmBootTask(context, listBookingSession).run();
    }
}
