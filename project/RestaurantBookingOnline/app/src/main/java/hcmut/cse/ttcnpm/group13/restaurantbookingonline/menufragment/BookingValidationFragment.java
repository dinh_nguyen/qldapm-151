package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.IOException;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.SuccessfulAnnounceFragment;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ReminderLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ScheduleClient;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.NetworkUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingValidationFragment extends Fragment {


    private final int VALIDATE_OK = 0;
    private final int INVALID_CODE = 1;
    private final int NOT_CONNECT_TO_SERVER = 2;

    private EditText mCodeValidationEditText = null;
    private TextView mNewEmailComing = null;
    private Button mConfirmBtn = null;

    private static final String ARG_STATE = "state";

    private ScheduleClient mScheduleClient = null;

    public BookingValidationFragment() {
        // Required empty public constructor
    }

    public static BookingValidationFragment newInstance(boolean isOpeningFromBooking) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_STATE, isOpeningFromBooking);

        BookingValidationFragment fragment = new BookingValidationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_booking_validation, container, false);
        if (rootView != null) {

            // Init schedule service
            mScheduleClient = new ScheduleClient(getActivity());
            mScheduleClient.doBindService();

            mCodeValidationEditText = (EditText) rootView.findViewById(R.id.editTxtCodeValidation);
            mNewEmailComing = (TextView) rootView.findViewById(R.id.txtFooterTopValidation);
            if (mNewEmailComing != null) {
                boolean isOpenFromBooking = getArguments().getBoolean(ARG_STATE, false);
                if (isOpenFromBooking) {
                    mNewEmailComing.setVisibility(View.GONE);
                } else {
                    mNewEmailComing.setVisibility(View.VISIBLE);
                }
            }

            mCodeValidationEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                }
            });

            mConfirmBtn = (Button) rootView.findViewById(R.id.btnConfirm);
            if (mConfirmBtn != null) {
                mConfirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkingCodeValidation();
                    }
                });
            }
        }
        return rootView;
    }

    @Override
    public void onStop() {
        if (mScheduleClient != null) {
            mScheduleClient.doUnBindService();
        }
        super.onStop();
    }

    private void checkingCodeValidation() {
        if (mCodeValidationEditText != null) {
            final String currentInputCode = mCodeValidationEditText.getText().toString();

            // We mustn't let code empty
            if (currentInputCode.equals("")) {
                Toast emptyCodeToast = Toast.makeText(getActivity().getApplicationContext(), R.string.toast_miss_code_validation, Toast.LENGTH_SHORT);
                emptyCodeToast.show();
            }
            // We will check from database
            else
            {
                new CheckingNetworkAndSendValidationCodeTask(currentInputCode).execute();
            }
        }
    }

    private class CheckingNetworkAndSendValidationCodeTask extends AsyncTask<Void, Void, Integer> {

        private String mValidationCode = "";
        private ProgressDialog mSendingValidationCodeProgressDialog = null;

        public CheckingNetworkAndSendValidationCodeTask(String aValidationCode)
        {
            mValidationCode = aValidationCode;
        }

        @Override
        protected void onPreExecute() {
            mSendingValidationCodeProgressDialog = ProgressDialog.show(getActivity(),"Validating Code","Please wait a few seconds...",true);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            boolean isConnected = NetworkUtils.isNetworkConnected(getActivity());
            int result = VALIDATE_OK;
            if (isConnected)
            {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("BookingInfor");
                query.whereEqualTo("Active", mValidationCode);
                try {
                    final ParseObject object = query.getFirst();
                    ParseObject booking = new ParseObject("BookingInfor");
                    booking.put("Active", mValidationCode);
                    booking.saveEventually(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null)
                            {
                                final String objectId = object.getObjectId();
                                boolean isRemind = object.getBoolean("IsRemind");
                                String deviceId = object.getString("DeviceID");
                                String androidId = BookingSessionLab.getDeviceId();

                                if (isRemind == true && androidId.equals(deviceId)) {
                                    HistoryLab historyLabInstance = HistoryLab.get(getActivity());
                                    ReminderLab reminderLabInstance = ReminderLab.get(getActivity());

                                    // Update state in history lab
                                    historyLabInstance.updateActivatedBooking(objectId);
                                    // Update in reminder list
                                    reminderLabInstance.updateBookingFromHistory(getActivity());

                                    int index = reminderLabInstance.getIndexFromIdBooking(objectId);
                                    if (index != -1) {
                                        mScheduleClient.setAlarmForNotification(index);
                                    }
                                }
                            }
                        }
                    });
                }
                catch (ParseException e)
                {
                    result = INVALID_CODE;
                }
            }
            else
            {
                result = NOT_CONNECT_TO_SERVER;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            mSendingValidationCodeProgressDialog.dismiss();
            switch (result)
            {
                case VALIDATE_OK:
                    Fragment fragment = new SuccessfulAnnounceFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    if (transaction != null) {
                        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                        transaction.replace(R.id.frame_container, fragment).commit();
                    }
                    break;
                case INVALID_CODE:
                    Toast errorValidationToast;
                    errorValidationToast = Toast.makeText(getActivity().getApplicationContext(), R.string.toast_invalid_code_validation, Toast.LENGTH_SHORT);
                    errorValidationToast.show();
                    break;
                case NOT_CONNECT_TO_SERVER:
                    Toast serverErrorToast = Toast.makeText(getActivity().getApplicationContext(), R.string.toast_server_error_code_validation, Toast.LENGTH_SHORT);
                    serverErrorToast.show();
                    break;
                default:
                    break;
            }
            // Can connect to the internet
//            if (result) {
//                ParseQuery<ParseObject> query = ParseQuery.getQuery("BookingInfor");
//                query.whereEqualTo("Active", mValidationCode);
//                query.getFirstInBackground(new GetCallback<ParseObject>() {
//                    @Override
//                    public void done(final ParseObject object, ParseException e) {
//                        // Find the code expected
//                        if (e == null) {
//                            ParseObject booking = new ParseObject("BookingInfor");
//                            booking.put("Active", mValidationCode);
//                            booking.saveEventually(new SaveCallback() {
//                                @Override
//                                public void done(ParseException e) {
//                                    if (e == null) {
//                                        boolean isRemind = object.getBoolean("IsRemind");
//                                        String deviceId = object.getString("DeviceID");
//                                        String androidId = BookingSessionLab.getDeviceId();
//                                        if (isRemind == true && androidId.equals(deviceId)) {
//                                            // Update state in history lab
//                                            HistoryLab.get(getActivity()).updateActivatedBooking(object.getObjectId());
//                                            // Update in reminder list
//                                            ReminderLab.updateBookingFromHistory(getActivity());
//
//                                            int index = ReminderLab.getIndexFromIdBooking(mValidationCode);
//                                            if (index != -1) {
//                                                mScheduleClient.setAlarmForNotification(index);
//                                            }
//                                        }
//                                    }
//                                }
//                            });
//
//                            // Go to successfully announcement fragment
//                            Fragment fragment = new SuccessfulAnnounceFragment();
//                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                            FragmentTransaction transaction = fragmentManager.beginTransaction();
//                            if (transaction != null) {
//                                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
//                                transaction.replace(R.id.frame_container, fragment).commit();
//                            }
//
//                        }
//                        // Invalid code
//                        else {
//                            Toast errorValidationToast;
//                            errorValidationToast = Toast.makeText(getActivity().getApplicationContext(), R.string.toast_invalid_code_validation, Toast.LENGTH_SHORT);
//                            errorValidationToast.show();
//                        }
//                    }
//                });
//            }
//            else
//            {
//                Toast serverErrorToast = Toast.makeText(getActivity().getApplicationContext(), R.string.toast_server_error_code_validation, Toast.LENGTH_SHORT);
//                serverErrorToast.show();
//            }
        }
    }
}
