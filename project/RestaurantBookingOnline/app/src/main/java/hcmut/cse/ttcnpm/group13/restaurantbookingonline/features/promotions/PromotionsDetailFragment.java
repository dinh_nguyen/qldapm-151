package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Locale;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionsDetailFragment extends Fragment {


    public PromotionsDetailFragment() {
        // Required empty public constructor
    }

    // Current promotion in fragment
    private Promotion mPromotion = null;

    // Current fields in fragment
    private ImageView mHeaderImageView = null;
    private TextView mTitleTextView = null;
    private ImageView mIconAuthorImageView = null;
    private TextView mAuthorTextView = null;
    private TextView mDatePromotionTextView = null;
    private TextView mExpiredDatePromotionTextView = null;
    private TextView mContentTextView = null;

    // Facebook share button
    private ShareButton mFacebookShareButton = null;
    
    private static final String ARG_Promotion_ID = "promotion_id";

    public static PromotionsDetailFragment newInstance(String promotionId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_Promotion_ID, promotionId);

        PromotionsDetailFragment fragment = new PromotionsDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            String promotionId = (String) args.getSerializable(ARG_Promotion_ID);
            mPromotion = PromotionsLab.get(getActivity()).getPromotion(promotionId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_promotion_detail, container, false);

        if (rootView != null) {
            mHeaderImageView = (ImageView) rootView.findViewById(R.id.promotionDetailImageView);
            if (mHeaderImageView != null) {
                Picasso.with(getContext())
                        .load(mPromotion.getIconId())
                        .placeholder(R.drawable.ic_place_holder)
                        .error(R.drawable.ic_place_holder)
                        .into(mHeaderImageView);
            }

            mTitleTextView = (TextView) rootView.findViewById(R.id.promotionDetailTitleTextView);
            if (mTitleTextView != null) {
                mTitleTextView.setText(mPromotion.getTitle());
            }

            mIconAuthorImageView = (ImageView) rootView.findViewById(R.id.promotionDetailLogoImageView);
            if (mIconAuthorImageView != null) {
                mIconAuthorImageView.setImageResource(mPromotion.getAuthorId());
            }

            mAuthorTextView = (TextView) rootView.findViewById(R.id.promotionDetailAuthorTextView);
            if (mAuthorTextView != null) {
                mAuthorTextView.setText(mPromotion.getAuthor());
            }

            mDatePromotionTextView = (TextView) rootView.findViewById(R.id.promotionDetailDateTextView);
            if (mDatePromotionTextView != null) {
                mDatePromotionTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy | kk:mm", Locale.ENGLISH)
                        .format(mPromotion.getDate().getTime()));
            }

            mExpiredDatePromotionTextView = (TextView) rootView.findViewById(R.id.promotionDetailExpiredDateTextView);
            if (mExpiredDatePromotionTextView != null) {
                mExpiredDatePromotionTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy | kk:mm", Locale.ENGLISH)
                        .format(mPromotion.getExpiredDate().getTime()));
            }

            mContentTextView = (TextView) rootView.findViewById(R.id.promotionDetailContentTextView);
            if (mContentTextView != null) {
                mContentTextView.setText(mPromotion.getContent());
            }

            mFacebookShareButton = (ShareButton)rootView.findViewById(R.id.btnFacebookShareNews);
            if (mFacebookShareButton != null)
            {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentTitle(mPromotion.getTitle())
                        .setContentDescription(mPromotion.getDescription())
                        .setContentUrl(Uri.parse(mPromotion.getPromotionLink()))
                        .setImageUrl(Uri.parse(mPromotion.getIconId()))
                        .build();

                mFacebookShareButton.setShareContent(content);
            }
        }
        return rootView;
    }
}

