package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

/**
 * Created by dinh on 11/21/2015.
 */
public class ScheduleClient {

    // The hook into our service
    private ScheduleService mBoundService;
    //  The context to start the service
    private Context mContext;
    // A flag if we are connected to the service or not
    private boolean mIsBound;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // This is called when the connection has been established,
            // giving us the service object we can use to interact with our service
            mBoundService = ((ScheduleService.ServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBoundService = null;
        }
    };

    public void setAlarmForNotification(int index) {
        mBoundService.setAlarm(index);
    }

    public ScheduleClient(Context context) {
        mContext = context;
    }

    public void doBindService() {
        // Establish a connection with our service
        mContext.bindService(new Intent(mContext, ScheduleService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    public void doUnBindService() {
        if (mIsBound) {
            // Detach our existing connection
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }
}
