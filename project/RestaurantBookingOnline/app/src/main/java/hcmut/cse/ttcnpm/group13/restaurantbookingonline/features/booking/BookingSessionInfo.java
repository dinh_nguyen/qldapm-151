package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/11/2015.
 */
public class BookingSessionInfo implements Serializable {

    public static final int EXPIRED = 0;
    public static final int INVALIDATE = 1;
    public static final int IN_COMING = 2;

    private String mId ="";
    private String mName = "";
    private String mEmail = "";
    private String mPhoneNumber = "";
    private Calendar mBookingDate = null;
    private Calendar mBookingTime = null;
    private int mNumberOfPeople = 0;
    private String mNote = "";
    private int mState = INVALIDATE;
    private MyUtils.MyPair mRemindInfo;

    public BookingSessionInfo() {
        mRemindInfo = new MyUtils.MyPair();
    }

    public BookingSessionInfo(String aId, String aName, String aEmail, String aPhoneNumber, String aBookingDate, String aBookingTime, int aNumberOfPeople,
                              String aNote, boolean aIsRemind, int aRemindInterval)
    {
        mId = aId;
        mName = aName;
        mEmail = aEmail;
        mPhoneNumber = aPhoneNumber;
        mNumberOfPeople = aNumberOfPeople;
        mNote = aNote;
        mRemindInfo = new MyUtils.MyPair();
        mRemindInfo.setKey(aIsRemind);
        mRemindInfo.setValue(aRemindInterval);

        try {
            mBookingDate = Calendar.getInstance();
            mBookingDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(aBookingDate));
            mBookingDate.set(Calendar.HOUR_OF_DAY, 0);
            mBookingDate.set(Calendar.MINUTE, 0);

            mBookingTime = Calendar.getInstance();
            mBookingTime.setTime(new SimpleDateFormat("kk:mm").parse(aBookingTime));
            mBookingTime.set(Calendar.YEAR, 2000);
            mBookingTime.set(Calendar.MONTH, 1);
            mBookingTime.set(Calendar.DAY_OF_MONTH, 1);
            mBookingTime.set(Calendar.SECOND,0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public BookingSessionInfo(String aId, String aName, String aEmail, String aPhoneNumber, String aBookingDate, String aBookingTime, int aNumberOfPeople,
                              String aNote, int aState, boolean aIsRemind, int aRemindInterval) {
        mId = aId;
        mName = aName;
        mEmail = aEmail;
        mPhoneNumber = aPhoneNumber;
        mNumberOfPeople = aNumberOfPeople;
        mNote = aNote;
        mState = aState;
        mRemindInfo = new MyUtils.MyPair();
        mRemindInfo.setKey(aIsRemind);
        mRemindInfo.setValue(aRemindInterval);

        try {
            mBookingDate = Calendar.getInstance();
            mBookingDate.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(aBookingDate));
            mBookingDate.set(Calendar.HOUR_OF_DAY, 0);
            mBookingDate.set(Calendar.MINUTE, 0);

            mBookingTime = Calendar.getInstance();
            mBookingTime.setTime(new SimpleDateFormat("kk:mm").parse(aBookingTime));
            mBookingTime.set(Calendar.YEAR, 2000);
            mBookingTime.set(Calendar.MONTH, 1);
            mBookingTime.set(Calendar.DAY_OF_MONTH, 1);
            mBookingTime.set(Calendar.SECOND,0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return mId;
    }
    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public Calendar getBookingDateOnly() {
        return mBookingDate;
    }

    public void setBookingDateOnly(Calendar bookingDate) {
        mBookingDate = bookingDate;
    }

    public Calendar getBookingTimeOnly() {
        return mBookingTime;
    }

    public Calendar getRealFullBookingDate() {
        Calendar date = null;
        if (mBookingDate != null && mBookingTime != null) {
            date = Calendar.getInstance();
            int year = getBookingYear();
            int month = getBookingMonth();
            int day = getBookingDay();
            int hour = getBookingHour();
            int minute = getBookingMinute();
            date.set(Calendar.YEAR, year);
            date.set(Calendar.MONTH, month);
            date.set(Calendar.DAY_OF_MONTH, day);
            date.set(Calendar.HOUR_OF_DAY, hour);
            date.set(Calendar.MINUTE, minute);
            date.set(Calendar.SECOND, 0);
        }
        return date;
    }

    public void setBookingTimeOnly(Calendar bookingTime) {
        mBookingTime = bookingTime;
    }

    public void setBookingYear(int year) {
        mBookingDate.set(Calendar.YEAR, year);
    }

    public void setBookingMonth(int month) {
        mBookingDate.set(Calendar.MONTH, month);
    }

    public void setBookingDay(int day) {
        mBookingDate.set(Calendar.DAY_OF_MONTH, day);
    }

    public void setBookingHour(int hour) {
        mBookingTime.set(Calendar.HOUR_OF_DAY, hour);
    }

    public void setBookingMinute(int minute) {
        mBookingTime.set(Calendar.MINUTE, minute);
    }

    public int getBookingYear() {
        return mBookingDate.get(Calendar.YEAR);
    }

    public int getBookingMonth() {
        return mBookingDate.get(Calendar.MONTH);
    }

    public int getBookingDay() {
        return mBookingDate.get(Calendar.DAY_OF_MONTH);
    }

    public int getBookingHour() {
        return mBookingTime.get(Calendar.HOUR_OF_DAY);
    }

    public int getBookingMinute() {
        return mBookingTime.get(Calendar.MINUTE);
    }

    public int getNumberOfPeople() {
        return mNumberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        mNumberOfPeople = numberOfPeople;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public int getState() {
        return mState;
    }

    public void setState(int state) {
        mState = state;
    }

    public MyUtils.MyPair getRemindInfo() {
        return mRemindInfo;
    }

    public void setRemindInfo(MyUtils.MyPair remindInfo) {
        mRemindInfo = remindInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookingSessionInfo that = (BookingSessionInfo) o;

        if (mNumberOfPeople != that.mNumberOfPeople) return false;
        if (mName != null ? !mName.equals(that.mName) : that.mName != null) return false;
        if (mEmail != null ? !mEmail.equals(that.mEmail) : that.mEmail != null) return false;
        if (mPhoneNumber != null ? !mPhoneNumber.equals(that.mPhoneNumber) : that.mPhoneNumber != null)
            return false;
        if (mBookingDate != null ? (mBookingDate.compareTo(that.mBookingDate) != 0) : that.mBookingDate != null)
            return false;
        if (mBookingTime != null ? (mBookingTime.compareTo(that.mBookingTime) != 0) : that.mBookingTime != null)
            return false;
        if (mNote != null ? !mNote.equals(that.mNote) : that.mNote != null) return false;
        return !(mRemindInfo != null ? !mRemindInfo.equals(that.mRemindInfo) : that.mRemindInfo != null);

    }

    @Override
    public int hashCode() {
        int result = mName != null ? mName.hashCode() : 0;
        result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
        result = 31 * result + (mPhoneNumber != null ? mPhoneNumber.hashCode() : 0);
        result = 31 * result + (mBookingDate != null ? mBookingDate.hashCode() : 0);
        result = 31 * result + (mBookingTime != null ? mBookingTime.hashCode() : 0);
        result = 31 * result + mNumberOfPeople;
        result = 31 * result + (mNote != null ? mNote.hashCode() : 0);
        result = 31 * result + (mRemindInfo != null ? mRemindInfo.hashCode() : 0);
        return result;
    }
}
