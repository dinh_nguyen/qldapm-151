package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.NetworkUtils;

/**
 * Created by dinh on 11/6/2015.
 */
public class FeedbackFragment extends Fragment {

    private RatingBar mRatingBar = null;

    private TextView mStringValueTextView = null;

    private EditText mNameEditText = null;
    private EditText mEmailEditText = null;
    private EditText mTitleEditText = null;
    private EditText mContentEditText = null;

    private Button mSubmitButton = null;

    private float mRatingValue = 0.0f;

    private long mLastClickTime = 0;

    public FeedbackFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
        if (rootView != null)
        {
            mRatingBar = (RatingBar)rootView.findViewById(R.id.ratingBarFeedback);
            mStringValueTextView = (TextView)rootView.findViewById(R.id.txtRatingValueFeedback);

            mNameEditText = (EditText)rootView.findViewById(R.id.nameFeedbackEditText);
            mEmailEditText = (EditText)rootView.findViewById(R.id.emailFeedbackEditText);
            mTitleEditText = (EditText)rootView.findViewById(R.id.titleFeedbackEditText);
            mContentEditText = (EditText)rootView.findViewById(R.id.contentFeedbackEditText);

            mSubmitButton = (Button)rootView.findViewById(R.id.btnSubmitFeedback);

            mStringValueTextView.setText(getStringFromValueRating(mRatingBar.getRating()));

            Drawable progress = mRatingBar.getProgressDrawable();
            DrawableCompat.setTint(progress, Color.parseColor("#129793"));

            mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    mStringValueTextView.setText(getStringFromValueRating(rating));
                    mRatingValue = rating;
                }
            });

            mTitleEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        v.setPressed(true);
                        showContentDialog();
                    }
                    return false;
                }
            });

            mContentEditText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return true;
                        }
                        mContentEditText.requestFocus();
                        mLastClickTime = SystemClock.elapsedRealtime();
                        v.setPressed(true);
                        showContentDialog();
                    }
                    return true;
                }
            });

            mSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new CheckingNetworkAndRunningFeedbackTask().execute();
                }
            });
        }
        return rootView;
    }

    String getStringFromValueRating(float ratingValue)
    {
        String strValue = "";
        if (ratingValue <= 1.0f)
        {
            strValue = "VERY BAD";
        }
        else if (ratingValue <= 2.0f)
        {
            strValue = "BAD";
        }
        else if (ratingValue <= 3.0f)
        {
            strValue = "NORMAL";
        }
        else if (ratingValue <= 4.0f)
        {
            strValue = "GOOD";
        }
        else
        {
            strValue = "EXCELLENT";
        }
        return strValue;
    }

    private class SuccessFeedbackDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            Fragment fragment = new HomeFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (transaction != null) {
                transaction.replace(R.id.frame_container, fragment).commit();
            }
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {

        }

        @Override
        public Activity getCurrentActivity() {
            return getActivity();
        }
    }

    private class ContentInputDialog extends MyUtils.MyAlertDialog {

        private EditText mContentDialogEditText = null;

        public EditText getDialogEditText() {
            return mContentDialogEditText;
        }

        @Override
        public Activity getCurrentActivity() {
            return getActivity();
        }

        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            if (mContentDialogEditText != null && mContentEditText != null) {
                mContentEditText.setText(mContentDialogEditText.getText().toString());
            }
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {
            mContentEditText.clearFocus();
        }

        @Override
        public AlertDialog createMyAlertDialog(String aTitle, String aMessage, String txtPositiveBtn, String txtNegativeBtn) {
            AlertDialog alertDialog = super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            final View dialogView = layoutInflater.inflate(R.layout.dialog_content_feedback, null);
            alertDialog.setView(dialogView);

            mContentDialogEditText = (EditText) dialogView.findViewById(R.id.contentDialogEditTextFeedback);
            mContentDialogEditText.requestFocus();

            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            return alertDialog;
        }
    }

    private void showContentDialog() {
        ContentInputDialog currentDialog = new ContentInputDialog();
        AlertDialog alertDialog = currentDialog.createMyAlertDialog(R.string.txt_content_dialog_title_feedback, R.string.txt_content_dialog_message_feedback, R.string.txt_OK, R.string.txt_Cancel);

        EditText currentEditText = currentDialog.getDialogEditText();
        currentEditText.setText(mContentEditText.getText());

        alertDialog.show();
    }

    private boolean isValidInput()
    {
        boolean result = true;

        if (mEmailEditText != null)
        {
            String email = mEmailEditText.getText().toString();

            final Pattern emailPattern = Pattern.compile("([a-zA-Z0-9_.+-]+)(@)([a-zA-Z0-9-]+)(\\.[a-zA-Z0-9-]+)*");
            final Matcher emailEditTextMatcher = emailPattern.matcher(email);


            if (!email.equals("") && emailEditTextMatcher.matches() == false) {
                Toast emailToast = Toast.makeText(getActivity(), R.string.toast_invalid_email_format, Toast.LENGTH_SHORT);
                emailToast.setMargin(0, -0.05f);
                emailToast.show();
                result = false;
            }
        }

        if (mContentEditText != null)
        {
            String content = mContentEditText.getText().toString();
            if (content.equals("")) {
                result = false;
                Toast.makeText(getActivity(),R.string.toast_empty_content_feedback,Toast.LENGTH_SHORT).show();
            }
        }
        return result;
    }

    private class CheckingNetworkAndRunningFeedbackTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            return NetworkUtils.isNetworkConnected(getActivity());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (isValidInput()) {
                if (result) {
                    String receiver = mEmailEditText.getText().toString();
                    String name = mNameEditText.getText().toString();
                    Calendar today = Calendar.getInstance();

                    new SendingFeedbackTask(receiver, name, today).execute();
                } else {
                    Toast.makeText(getActivity(), R.string.toast_error_confirm_sending_mail, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class SendingFeedbackTask extends AsyncTask<Void, Void, Void> {

        private String mName;
        private String mReceiver;

        private float mRatingValueDialog = 0.0f;
        private String mTitle;
        private String mContent;
        private Calendar mDate;

        private ProgressDialog mSendingFeedbackProgressDialog = null;

        public SendingFeedbackTask(String receiver, String name, Calendar date)
        {
            super();
            mReceiver = receiver;
            mName = name;
            mDate = date;
        }

        @Override
        protected void onPreExecute() {
            mSendingFeedbackProgressDialog = ProgressDialog.show(getActivity(),"Sending Feedback","Please wait a few second...",true);

            mRatingValueDialog = mRatingValue;
            mTitle = mTitleEditText.getText().toString();
            mContent = mContentEditText.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {

            final ParseObject objectFeedbackInfor = new ParseObject("FeedbackInfor");

            String dateStr = new SimpleDateFormat("dd/MM/yyyy").format(mDate.getTime());
            String timeStr = new SimpleDateFormat("kk:mm").format(mDate.getTime());

            objectFeedbackInfor.put("Rating",mRatingValueDialog);
            objectFeedbackInfor.put("Name",mName);
            objectFeedbackInfor.put("Email",mReceiver);
            objectFeedbackInfor.put("Title",mTitle);
            objectFeedbackInfor.put("Date",dateStr);
            objectFeedbackInfor.put("Time",timeStr);
            objectFeedbackInfor.put("Content",mContent);

            objectFeedbackInfor.saveEventually();


            // Sending email to receiver
            if (!mReceiver.equals("")) {
                String username = getString(R.string.mail_sender_username);
                String password = getString(R.string.mail_sender_password);
                String receiver = mReceiver;
                String title = getString(R.string.mail_title_validation);

                String textMessage = "Dear " + "<b>" + mName + "</b>" +"," + "<br/><br/>" +
                        "Really thanks for your feedback to improve our restaurant service." + "<br/><br/>" +
                        "Have a good day !!!";

                Message feedbackMessage = NetworkUtils.createGmailMessage(getActivity(), username, password, receiver, title, textMessage);

                try {
                    Transport.send(feedbackMessage);
                } catch (MessagingException e) {
                    Toast.makeText(getActivity(), R.string.toast_error_sending_mail, Toast.LENGTH_SHORT).show();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mSendingFeedbackProgressDialog.dismiss();
            AlertDialog dialog = new SuccessFeedbackDialog().createMyAlertDialog(R.string.txt_title_success_dialog_feedback,R.string.txt_message_success_dialog_feedback,R.string.txt_OK,-1);
            dialog.show();
        }
    }
}
