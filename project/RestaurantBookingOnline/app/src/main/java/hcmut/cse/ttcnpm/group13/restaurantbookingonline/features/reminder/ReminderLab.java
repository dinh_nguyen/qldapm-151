package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.tinyDB.TinyDB;

/**
 * Created by dinh on 11/22/2015.
 */
public class ReminderLab {

    public static final String ARG_LIST_REMINDER_BOOT = "list_reminder_boot";

    public static final String ARG_LIST_REMINDER_APP = "list_reminder_app";

    private static boolean isInit = false;

    private static ReminderLab sReminderLab;

    private static List<BookingSessionInfo> mReminderList = null;

    private ReminderLab(Context aContext) {
        if (mReminderList == null) {
            mReminderList = new ArrayList<>();
        }

        isInit = true;

        // Get data form local database
        TinyDB tinyDB = new TinyDB(aContext);
        ArrayList<Object> listInAppFromDB = tinyDB.getListObject(ARG_LIST_REMINDER_APP, BookingSessionInfo.class);
        for (Object object : listInAppFromDB) {
            insertToReminderList(object);
        }
    }

    public boolean insertToReminderList(Object o) {
        boolean flag = false;
        BookingSessionInfo session = (BookingSessionInfo) o;
        if (session != null) {
            if (!mReminderList.contains(session) && session.getState() == BookingSessionInfo.IN_COMING) {
                flag = true;
                mReminderList.add(session);
            }
        }
        return flag;
    }

    public static ReminderLab get(Context aContext) {
        if (sReminderLab == null) {
            sReminderLab = new ReminderLab(aContext);
        }
        return sReminderLab;
    }

    public List<BookingSessionInfo> getReminderList() {
        return mReminderList;
    }

    public static boolean isInit() {
        return isInit;
    }

    public int getIndexFromIdBooking(String id)
    {
        int flag = -1;
        for (int i = mReminderList.size() - 1; i >= 0;i--)
        {
            if (mReminderList.get(i).getId().equals(id))
            {
                flag = i;
                break;
            }
        }
        return flag;
    }

    public void updateBookingState() {
        long todayMillis = Calendar.getInstance().getTimeInMillis();

        for (BookingSessionInfo session : mReminderList) {
            int state = session.getState();
            if (state != BookingSessionInfo.EXPIRED) {
                long sessionMillis = session.getRealFullBookingDate().getTimeInMillis();
                if (todayMillis > sessionMillis) {
                    session.setState(BookingSessionInfo.EXPIRED);
                }
            }
        }

        for (int i = mReminderList.size() - 1; i >= 0; i--) {
            if (mReminderList.get(i).getState() == BookingSessionInfo.EXPIRED) {
                mReminderList.remove(i);
            }
        }
    }

    public void updateIncomingBookingFromHistory(Context aContext) {
        List<BookingSessionInfo> historyList = HistoryLab.get(aContext).getBookedList();
        for (BookingSessionInfo session : historyList) {
            ReminderLab.get(aContext).insertToReminderList(session);
        }
    }

    public void updateBookingFromHistory(Context aContext) {
        ReminderLab.get(aContext).updateIncomingBookingFromHistory(aContext);
        ReminderLab.get(aContext).updateBookingState();
    }

    public void reNewBookingListFromHistory(Context aContext)
    {
        if (mReminderList != null)
        {
            // Clear old reminder list
            mReminderList.clear();

            List<BookingSessionInfo> bookedList = HistoryLab.get(aContext).getBookedList();
            for (BookingSessionInfo session: bookedList)
            {
                insertToReminderList(session);
            }
        }
    }
}
