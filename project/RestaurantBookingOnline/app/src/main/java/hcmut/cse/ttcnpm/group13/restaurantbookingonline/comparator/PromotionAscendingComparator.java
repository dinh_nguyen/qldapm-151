package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.Promotion;

/**
 * Created by dinh on 11/22/2015.
 */
public class PromotionAscendingComparator implements Comparator<Promotion> {
    @Override
    public int compare(Promotion lhs, Promotion rhs) {
        long lhsMillis = lhs.getExpiredDate().getTimeInMillis();
        long rhsMillis = rhs.getExpiredDate().getTimeInMillis();

        if (lhsMillis > rhsMillis) return 1;
        else if (lhsMillis == rhsMillis) return 0;
        else return -1;
    }
}
