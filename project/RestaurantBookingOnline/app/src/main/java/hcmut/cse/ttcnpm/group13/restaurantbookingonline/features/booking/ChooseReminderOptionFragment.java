package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseReminderOptionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseReminderOptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseReminderOptionFragment extends Fragment {

    public ChooseReminderOptionFragment() {
    }

    // debug TAG
    final private String TAG = "ChooseReminderOption";

    private ToggleButton mReminderOptionToggleBtn;
    private MultiStateToggleButton mMultiTimeSelectionBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choose_remind_option_booking_session, container, false);
        if (rootView != null) {
            mMultiTimeSelectionBtn = (MultiStateToggleButton) rootView.findViewById(R.id.multiTimeSelectionBtn);
            if (mMultiTimeSelectionBtn != null) {
                setValueInMenuSelection(mMultiTimeSelectionBtn, BaseBookingActivity.getCurrentSession(getActivity()).getRemindInfo().getValue());

                mMultiTimeSelectionBtn.setOnValueChangedListener(new org.honorato.multistatetogglebutton.ToggleButton.OnValueChangedListener() {
                    @Override
                    public void onValueChanged(int value) {
                        int minute = getMinuteInMenuSelectionIndex(mMultiTimeSelectionBtn, value);
                        //BaseBookingActivity.mRemindInfo.setValue(minute);
                        BaseBookingActivity.getCurrentSession(getActivity()).getRemindInfo().setValue(minute);
                    }
                });
            }

            mReminderOptionToggleBtn = (ToggleButton) rootView.findViewById(R.id.toggleBtnReminderOption);
            if (mReminderOptionToggleBtn != null) {
                // Set multi time selection visible depend on reminder option
                boolean isReminderActive = BaseBookingActivity.getCurrentSession(getActivity()).getRemindInfo().getKey();
                setButtonVisible(mMultiTimeSelectionBtn, isReminderActive);
                mReminderOptionToggleBtn.setChecked(isReminderActive);

                mReminderOptionToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        setButtonVisible(mMultiTimeSelectionBtn, isChecked);
                        BaseBookingActivity.getCurrentSession(getActivity()).getRemindInfo().setKey(isChecked);
                        if (isChecked == true) {
                            int minute = getMinuteInMenuSelectionIndex(mMultiTimeSelectionBtn, mMultiTimeSelectionBtn.getValue());
                            //BaseBookingActivity.mRemindInfo.setValue(minute);
                            BaseBookingActivity.getCurrentSession(getActivity()).getRemindInfo().setValue(minute);
                        }
                    }
                });
            }
        }
        return rootView;
    }

    private void setButtonVisible(LinearLayout button, boolean isVisible) {
        if (button != null) {
            if (isVisible == true) {
                button.setVisibility(View.VISIBLE);
            } else {
                button.setVisibility(View.GONE);
            }
        }
    }

    private int getMinuteInMenuSelectionIndex(MultiStateToggleButton button, int selIndex) {
        int minutes = -1;
        if (button != null) {
            switch (selIndex) {
                case 0:
                    minutes = 1;
                    break;
                case 1:
                    minutes = 15;
                    break;
                case 2:
                    minutes = 60;
                    break;
                case 3:
                    minutes = 1440;
                    break;
            }
        }
        return minutes;
    }

    private void setValueInMenuSelection(MultiStateToggleButton button, int varMinute) {
        if (button != null) {
            switch (varMinute) {
                case 1:
                    button.setValue(0);
                    break;
                case 15:
                    button.setValue(1);
                    break;
                case 60:
                    button.setValue(2);
                    break;
                case 1440:
                    button.setValue(3);
                    break;
                default:
                    button.setValue(0);
                    break;
            }
        }
    }
}
