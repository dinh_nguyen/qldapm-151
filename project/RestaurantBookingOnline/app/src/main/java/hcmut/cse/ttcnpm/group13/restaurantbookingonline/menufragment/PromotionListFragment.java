package hcmut.cse.ttcnpm.group13.restaurantbookingonline.menufragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.cache.PreCachingLayoutManager;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.Promotion;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsPagerActivity;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.DeviceUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/6/2015.
 */
public class PromotionListFragment extends Fragment {

    public PromotionListFragment() {
    }

    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    private RecyclerView mPromotionRecyclerView;
    private PromotionAdapter mPromotionAdapter;

    // Variable to prevent user click many times
    private long mLastClickTime = 0;

    private Calendar todayCalendar = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.booking_menuRefresh:
                Log.d("Menu", "onOptionItemSelected In PromotionFragment");
                new UpdatePromotionListTask().execute();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_promotions_list, container, false);

        mPromotionRecyclerView = (RecyclerView) rootView.findViewById(R.id.promotion_recycler_view);
        if (mPromotionRecyclerView != null) {
            PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
            layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
            mPromotionRecyclerView.setLayoutManager(layoutManager);
            updateUI();
        }

        return rootView;
    }

    private void updateUI() {
        if (mPromotionAdapter == null) {
            PromotionsLab promotionLab = PromotionsLab.get(getActivity());
            List<Promotion> promotionList = promotionLab.getPromotionsList();
            mPromotionAdapter = new PromotionAdapter(promotionList);
            mPromotionRecyclerView.setAdapter(mPromotionAdapter);
        }
        else
        {
            PromotionsLab promotionLab = PromotionsLab.get(getActivity());
            List<Promotion> promotionList = promotionLab.getPromotionsList();
            mPromotionAdapter.notifyDataSetChanged();
            mPromotionAdapter.notifyItemRangeChanged(0, promotionList.size());
        }
    }

    private class UpdatePromotionListTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            while(PromotionsLab.isLoadingData());
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Log.d("Menu", "Adapter In PromotionFragment Updated");
            updateUI();
        }
    }

    private class PromotionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Promotion mPromotion;

        private ImageView mImageView;
        private TextView mTitleTextView;
        private TextView mDescriptionTextView;
        private TextView mDateTextView;
        private TextView mInProgressDayTextView;
        private TextView mNewTextView;

        private View mBackground;

        public View getBackground() {
            return mBackground;
        }

        public void setBackground(View background) {
            mBackground = background;
        }

        public PromotionHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mBackground = itemView;

            mImageView = (ImageView) itemView.findViewById(R.id.list_item_promotion_icon_image_view);
            mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_promotion_title_text_view);
            mDescriptionTextView = (TextView)itemView.findViewById(R.id.list_item_promotion_description_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_promotion_date_text_view);
            mInProgressDayTextView = (TextView) itemView.findViewById(R.id.list_item_promotion_date_in_progress_text_view);
            mNewTextView = (TextView) itemView.findViewById(R.id.list_item_promotion_is_new_text_view);
        }

        public void bindPromotion(Promotion promotion) {
            mPromotion = promotion;
            Picasso.with(getContext())
                    .load(mPromotion.getIconId())
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(mImageView);
            mTitleTextView.setText(mPromotion.getTitle());
            mDescriptionTextView.setText(mPromotion.getDescription());

            StringBuilder compareAuthorDateStr = MyUtils.getStringAfterCompare(mPromotion.getDate(), todayCalendar);

            if (compareAuthorDateStr.indexOf("second") != -1) {
                compareAuthorDateStr = new StringBuilder("A minute ago");
            }
            mDateTextView.setText(compareAuthorDateStr);

            StringBuilder compareExpiredDateStr = MyUtils.getStringAfterCompareExpiredDate(mPromotion.getExpiredDate(), todayCalendar);

            if (compareExpiredDateStr.indexOf("second") != -1) {
                compareExpiredDateStr = new StringBuilder("In a minute");
            }

            mInProgressDayTextView.setText(compareExpiredDateStr);

            if (mPromotion.isNew()) {
                mNewTextView.setVisibility(View.VISIBLE);
            } else {
                mNewTextView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View v) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            // highlight selected item
            int pos = getAdapterPosition();

            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
                mBackground.setSelected(false);
            } else {
                selectedItems.put(pos, true);
                mBackground.setSelected(true);
            }

            // Set new label invisible when clicked
            Intent intent = PromotionsPagerActivity.newIntent(getActivity(), mPromotion.getId());
            startActivity(intent);
        }
    }

    private class PromotionAdapter extends RecyclerView.Adapter<PromotionHolder> {

        private List<Promotion> mPromotionList;

        public PromotionAdapter(List<Promotion> promotionList) {
            mPromotionList = promotionList;
        }

        @Override
        public void onBindViewHolder(PromotionHolder holder, int position) {
            Promotion promotion = mPromotionList.get(position);
            holder.bindPromotion(promotion);
            holder.mBackground.setSelected(selectedItems.get(position, false));
        }

        @Override
        public PromotionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_promotion, parent, false);
            PromotionHolder holder = new PromotionHolder(view);
            return holder;
        }

        @Override
        public int getItemCount() {
            return mPromotionList.size();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
}
