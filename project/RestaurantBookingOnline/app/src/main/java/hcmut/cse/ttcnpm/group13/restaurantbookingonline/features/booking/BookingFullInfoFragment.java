package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookingFullInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookingFullInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingFullInfoFragment extends Fragment {

    public BookingFullInfoFragment() {
    }

    private TextView mFooterTextView = null;

    private TextView mDateFieldTextView = null;
    private TextView mTimeFieldTextView = null;
    private TextView mEmailFieldTextView = null;
    private TextView mPhoneFieldTextView = null;
    private TextView mAttendantsFieldTextView = null;
    private TextView mReminderFieldTextView = null;
    private TextView mNoteFieldTextView = null;

    BookingSessionInfo mBookingInfo = null;

    private boolean isVisibleFooterTextView = true;

    private static final String ARGS_BOOKING_SESSION = "booking_session";

    public static BookingFullInfoFragment newInstance(BookingSessionInfo session) {
        Bundle args = new Bundle();
        args.putSerializable(ARGS_BOOKING_SESSION, session);

        BookingFullInfoFragment fragment = new BookingFullInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setVisibleFooterTextView(boolean aVisible) {
        isVisibleFooterTextView = aVisible;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_confirm, container, false);
        if (rootView != null) {

            mBookingInfo = (BookingSessionInfo) getArguments().getSerializable(ARGS_BOOKING_SESSION);

            mFooterTextView = (TextView) rootView.findViewById(R.id.txt_view_confirm_footer);
            if (mFooterTextView != null) {
                mFooterTextView.setText(getString(R.string.txt_confirm_footer_top));
                if (isVisibleFooterTextView == true) {
                    mFooterTextView.setVisibility(View.VISIBLE);
                } else {
                    mFooterTextView.setVisibility(View.GONE);
                }
            }

            mDateFieldTextView = (TextView) rootView.findViewById(R.id.contentDateOnlyFieldTextView);
            if (mDateFieldTextView != null) {
                Calendar date = mBookingInfo.getBookingDateOnly();
                if (date != null) {
                    mDateFieldTextView.setText(new SimpleDateFormat("EEEE, MMMM d, yyyy")
                            .format(date.getTime()));
                }
            }

            mTimeFieldTextView = (TextView) rootView.findViewById(R.id.contentTimeOnlyFieldTextView);
            if (mTimeFieldTextView != null) {
                Calendar time = mBookingInfo.getBookingTimeOnly();
                if (time != null) {
                    mTimeFieldTextView.setText(new SimpleDateFormat("kk:mm")
                            .format(time.getTime()));
                }
            }

            mEmailFieldTextView = (TextView) rootView.findViewById(R.id.contentEmailFieldTextView);
            if (mEmailFieldTextView != null) {
                mEmailFieldTextView.setText(mBookingInfo.getEmail());
            }

            mPhoneFieldTextView = (TextView) rootView.findViewById(R.id.contentPhoneFieldTextView);
            if (mPhoneFieldTextView != null) {
                mPhoneFieldTextView.setText(mBookingInfo.getPhoneNumber());
            }

            mAttendantsFieldTextView = (TextView) rootView.findViewById(R.id.contentAttendantsFieldTextView);
            if (mAttendantsFieldTextView != null) {
                String attendantNumber = Integer.toString(mBookingInfo.getNumberOfPeople());
                String attendantStr = attendantNumber + " " + "people";
                mAttendantsFieldTextView.setText(attendantStr);
            }

            mReminderFieldTextView = (TextView) rootView.findViewById(R.id.contentReminderNumFieldTextView);
            if (mReminderFieldTextView != null) {
                MyUtils.MyPair reminderData = mBookingInfo.getRemindInfo();
                if (reminderData.getKey() == false) {
                    mReminderFieldTextView.setText("None");
                } else {
                    String timeStr = MyUtils.getStringFromReminderTime(reminderData.getValue());
                    mReminderFieldTextView.setText("Before " + timeStr);
                }
            }

            mNoteFieldTextView = (TextView) rootView.findViewById(R.id.contentNoteFieldTextView);
            if (mNoteFieldTextView != null) {
                mNoteFieldTextView.setText(mBookingInfo.getNote());
            }
        }
        return rootView;
    }
}
