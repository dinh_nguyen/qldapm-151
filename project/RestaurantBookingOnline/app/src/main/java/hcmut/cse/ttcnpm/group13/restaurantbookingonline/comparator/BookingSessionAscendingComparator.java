package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/22/2015.
 */
public class BookingSessionAscendingComparator implements Comparator<BookingSessionInfo> {
    @Override
    public int compare(BookingSessionInfo lhs, BookingSessionInfo rhs) {

        int lhsState = lhs.getState();
        int rhsState = rhs.getState();
        int compareState = lhsState - rhsState;
        if (compareState != 0) return compareState;

        long lhsMillis = lhs.getRealFullBookingDate().getTimeInMillis();
        long rhsMillis = rhs.getRealFullBookingDate().getTimeInMillis();

        if (lhsMillis > rhsMillis) return 1;
        else if (lhsMillis == rhsMillis) return 0;
        else return -1;
    }
}
