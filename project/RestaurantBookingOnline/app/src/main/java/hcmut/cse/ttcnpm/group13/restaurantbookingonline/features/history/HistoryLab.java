package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history;

import android.content.Context;
import android.provider.Settings;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator.BookingSessionAscendingComparator;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator.BookingSessionDescendingComparator;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;

/**
 * Created by dinh on 11/20/2015.
 */
public class HistoryLab {

    private static HistoryLab sHistoryLab;

    private static boolean isLoadingData = false;

    private static boolean isInit = false;

    // List object from parse server
    List<ParseObject> mHistoryParseObjectList;

    private static List<BookingSessionInfo> mBookedList;

    private static String androidId = "";

    public static boolean isLoadingData()
    {
        return isLoadingData;
    }

    private HistoryLab(Context aContext) {

        isInit = true;
        mBookedList = new ArrayList<>();
        androidId = Settings.Secure.getString(aContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        updateDataLocalFromServer(aContext);
    }

    public static boolean isInit() {
        return isInit;
    }

    public void sortAscendingBookedList() {
        Collections.sort(mBookedList, new BookingSessionAscendingComparator());
    }

    public void sortDescendingBookedList() {
        Collections.sort(mBookedList, new BookingSessionDescendingComparator());
    }

    // Customize it if you want different sort
    public static HistoryLab get(Context aContext) {
        if (sHistoryLab == null) {
            sHistoryLab = new HistoryLab(aContext);
        }
        return sHistoryLab;
    }

    public List<BookingSessionInfo> getBookedList() {
        return mBookedList;
    }

    public BookingSessionInfo getBookedSession(String id) {
        for (BookingSessionInfo session : mBookedList) {
            if (session.getId().equals(id)) {
                return session;
            }
        }
        return null;
    }

    public boolean removeBookedSession(String id) {
        BookingSessionInfo session = getBookedSession(id);
        if (session != null) {
            mBookedList.remove(session);
            return true;
        }
        return false;
    }

    public boolean addBookedSession(BookingSessionInfo bookingSessionInfo) {
        boolean flag = false;
        if (!mBookedList.contains(bookingSessionInfo)) {
            flag = true;
            mBookedList.add(bookingSessionInfo);
        }
        return flag;
    }

    public int getCounterIsComingBooking() {
        int count = 0;
        for (BookingSessionInfo session : mBookedList) {
            if (session.getState() == BookingSessionInfo.IN_COMING) {
                count += 1;
            }
        }
        return count;
    }

    public void updateBookingState() {
        long todayMillis = Calendar.getInstance().getTimeInMillis();
        for (BookingSessionInfo session : mBookedList) {
            int state = session.getState();
            if (state != BookingSessionInfo.EXPIRED) {
                long sessionMillis = session.getRealFullBookingDate().getTimeInMillis();
                if (todayMillis > sessionMillis) {
                    session.setState(BookingSessionInfo.EXPIRED);
                }
            }
        }
    }

    public void updateActivatedBooking(String id)
    {
        for (BookingSessionInfo session : mBookedList) {
            if (session.getId().equals(id)) {
                if (session.getState() == BookingSessionInfo.INVALIDATE)
                {
                    session.setState(BookingSessionInfo.IN_COMING);
                }
                break;
            }
        }
    }

    public void updateDataLocalFromServer(final Context aContext) {
        isLoadingData = true;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("BookingInfor");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                ParseObject.unpinAllInBackground("BookingInformationParse", new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        ParseObject.pinAllInBackground("BookingInformationParse", objects);
                        updateDataFromCache(aContext);
                    }
                });
            }
        });
    }

    public void updateDataFromCache(final Context aContext) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("BookingInfor");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                mHistoryParseObjectList = objects;
                if (objects != null) {
                    updateDataLocal(aContext);
                    isLoadingData = false;
                }
            }
        });
    }

    public void updateDataLocal(Context aContext) {
        // Clear old
        if (mBookedList != null) {
            mBookedList.clear();
        }

        for (ParseObject newsRecord: mHistoryParseObjectList) {
            String deviceIdSession = newsRecord.getString("DeviceID");
            if (deviceIdSession.equals(androidId)) {
                String id = newsRecord.getObjectId();
                String name = newsRecord.getString("Ten");
                String email = newsRecord.getString("email");
                String phoneNumber = newsRecord.getString("phone");
                String bookingDate = newsRecord.getString("Date");
                String bookingTime = newsRecord.getString("Time");
                int attendants = newsRecord.getNumber("SoNguoi").intValue();
                String note = newsRecord.getString("Note");

                Boolean isRemind = newsRecord.getBoolean("IsRemind");
                int remindInterval = newsRecord.getNumber("RemindInterval").intValue();

                String validateCode = newsRecord.getString("Active");
                int state = BookingSessionInfo.INVALIDATE;
                if ( validateCode.equals("Actived"))
                {
                    state = BookingSessionInfo.IN_COMING;
                }
                else
                {
                    state = BookingSessionInfo.INVALIDATE;
                }

                BookingSessionInfo session = new BookingSessionInfo(id, name, email, phoneNumber, bookingDate, bookingTime, attendants, note, state, isRemind, remindInterval);
                mBookedList.add(session);
            }
        }
        updateBookingState();
        Collections.sort(mBookedList,new BookingSessionDescendingComparator());
    }
}
