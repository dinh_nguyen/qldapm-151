package hcmut.cse.ttcnpm.group13.restaurantbookingonline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.facebook.appevents.AppEventsLogger;
import com.parse.Parse;

import java.util.List;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.history.HistoryLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.NewsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.Promotion;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions.PromotionsLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ReminderLab;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder.ScheduleClient;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.NetworkUtils;

public class SplashScreenActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    private long mStartTimestamp;
    //Handler handler;

    @Override
    protected void onResume() {
        super.onResume();
        // Log 'install' and 'app' active App Events
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*
         * Showing splash screen with a timer. Using AsyncTask
         */
        mStartTimestamp = System.currentTimeMillis();
        new PrefetchData().execute();
    }

    private class PrefetchData extends AsyncTask<Void, Void, Boolean> {

        private ScheduleClient mScheduleClient = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mScheduleClient = new ScheduleClient(getApplicationContext());
            mScheduleClient.doBindService();
            // before making https call
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final Context context = getApplicationContext();
            boolean isConnected = NetworkUtils.isNetworkConnected(context);
            // If can connect to internet, establish server and get data

            if (isConnected) {

                // Init data from server
                NewsLab.get(context);
                PromotionsLab.get(context);
                HistoryLab.get(context);

                // If not finish load news and promotions, keep stay in background
                while (NewsLab.isLoadingData());
                while (PromotionsLab.isLoadingData());
                while (HistoryLab.isLoadingData());

                ReminderLab.get(context).reNewBookingListFromHistory(context);
                List<BookingSessionInfo> reminderList = ReminderLab.get(context).getReminderList();
                for (int i = reminderList.size() - 1; i >= 0; i--)
                {
                    mScheduleClient.setAlarmForNotification(i);
                }
            }
            return isConnected;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            //super.onPostExecute(success);
            mScheduleClient.doUnBindService();
            if (success) {
                startHomeActivity();

                // Use this when we start app from time out (don't care connect)
//                long timeStop = System.currentTimeMillis();
//                long delta = timeStop - mStartTimestamp;
//                // If do in background (connect server) exceed SPLASH TIME
//                if (delta >= SPLASH_TIME_OUT) {
//                    startHomeActivity();
//                }
//                // Else we start home activity when reach SPLASH TIME
//                else {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startHomeActivity();
//                        }
//                    }, SPLASH_TIME_OUT - delta);
//                }
            }
            else
            {
                AlertDialog noConnectDialog =
                        new NoConnectionDialog().createMyAlertDialog(R.string.txt_no_connect_dialog_title, R.string.txt_no_connect_dialog_message, R.string.txt_OK, -1);
                noConnectDialog.show();
            }
        }
    }

    private void startHomeActivity() {
        Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
        startActivity(i);

        // close this activity
        finish();
    }

    private class NoConnectionDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            getCurrentActivity().finish();
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {

        }

        @Override
        public Activity getCurrentActivity() {
            return SplashScreenActivity.this;
        }
    }
}
