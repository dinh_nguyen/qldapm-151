package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Calendar;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;
import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.MyUtils;

/**
 * Created by dinh on 11/10/2015.
 */
public class DatePickerFragment extends DialogFragment {

    public static final String EXTRA_DATE =
            "hcmut.cse.ttcnpm.group13.restaurantbookingonline.booking_date";

    private static final String ARG_DATE = "booking_date";

    private MaterialCalendarView mDatePicker;

    public static DatePickerFragment newInstance(Calendar date) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, date);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void sendResult(int resultCode, Calendar date) {
        if (getTargetFragment() == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);

        getTargetFragment()
                .onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar tomorrow = Calendar.getInstance();
        //tomorrow.add(Calendar.DAY_OF_YEAR, 1);
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);

        Calendar calendar = (Calendar) getArguments().getSerializable(ARG_DATE);

        if (calendar == null) {
            // Set default date to specific date
            calendar = tomorrow;
        }

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        AlertDialog alertDialog = null;
        if (layoutInflater != null) {
            View v = layoutInflater.inflate(R.layout.dialog_date, null);
            mDatePicker = (MaterialCalendarView) v.findViewById(R.id.dialog_date_date_picker);
            mDatePicker.setDateSelected(calendar, true);

            // Set min date to tomorrow
            mDatePicker.setMinimumDate(tomorrow);

            alertDialog = new MyDateDialog().createMyAlertDialog(R.string.txt_date_picker_choose_time_title, R.string.txt_date_picker_choose_time_message, R.string.txt_OK, 0);
            alertDialog.setView(v);
        }
        return alertDialog;
    }

    // Use this when we want default time picker android

//    private void fixUpDatePickerCalendarView(Calendar date) {
//        // Workaround for CalendarView bug relating to setMinDate():
//        // https://code.google.com/p/android/issues/detail?id=42750
//        // Set then reset the date on the calendar so that it properly
//        // shows today's date. The choice of 24 months is arbitrary.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            final CalendarView cal = mDatePicker.getCalendarView();
//            if (cal != null) {
//                date.add(Calendar.MONTH, 24);
//                cal.setDate(date.getTimeInMillis(), false, true);
//                date.add(Calendar.MONTH, -24);
//                cal.setDate(date.getTimeInMillis(), false, true);
//            }
//        }
//    }


    class MyDateDialog extends MyUtils.MyAlertDialog {
        @Override
        public void onClickPositiveButton(DialogInterface dialog, int id) {
            Calendar date = mDatePicker.getSelectedDate().getCalendar();
            sendResult(Activity.RESULT_OK, date);
        }

        @Override
        public void onClickNegativeButton(DialogInterface dialog, int id) {

        }

        @Override
        public Activity getCurrentActivity() {
            return DatePickerFragment.this.getActivity();
        }

        @Override
        public AlertDialog createMyAlertDialog(String aTitle, String aMessage, String txtPositiveBtn, String txtNegativeBtn) {
            return super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
        }

        @Override
        public AlertDialog createMyAlertDialog(int aTitle, int aMessage, int txtPositiveBtn, int txtNegativeBtn) {
            return super.createMyAlertDialog(aTitle, aMessage, txtPositiveBtn, txtNegativeBtn);
        }
    }
}
