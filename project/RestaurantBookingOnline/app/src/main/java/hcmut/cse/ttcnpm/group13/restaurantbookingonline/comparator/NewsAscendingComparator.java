package hcmut.cse.ttcnpm.group13.restaurantbookingonline.comparator;

import java.util.Comparator;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.news.News;

/**
 * Created by dinh on 11/22/2015.
 */
public class NewsAscendingComparator implements Comparator<News> {
    @Override
    public int compare(News lhs, News rhs) {
        long lhsMillis = lhs.getDate().getTimeInMillis();
        long rhsMillis = rhs.getDate().getTimeInMillis();

        if (lhsMillis > rhsMillis) return 1;
        else if (lhsMillis == rhsMillis) return 0;
        else return -1;
    }
}
