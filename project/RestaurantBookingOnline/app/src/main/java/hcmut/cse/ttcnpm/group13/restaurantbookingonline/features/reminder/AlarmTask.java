package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/21/2015.
 */
public class AlarmTask implements Runnable {

    private final int MINUTE_TO_MILLI_SECOND = 60 * 1000;

    // index of list in coming booking session
    private int mIndexBookedList;
    // android system alarm manager
    private AlarmManager mAlarmManager;

    private Context mContext;

    public AlarmTask(Context aContext, int index) {
        this.mContext = aContext;
        this.mAlarmManager = (AlarmManager) aContext.getSystemService(Context.ALARM_SERVICE);
        this.mIndexBookedList = index;
    }

    @Override
    public void run() {

        Intent intent = NotifyService.newIntent(mContext, mIndexBookedList);
        PendingIntent pendingIntent = PendingIntent.getService(mContext, mIndexBookedList, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set as an alarm
        BookingSessionInfo session = ReminderLab.get(mContext).getReminderList().get(mIndexBookedList);
        long timeReminderMillis = session.getRealFullBookingDate().getTimeInMillis() - session.getRemindInfo().getValue() * MINUTE_TO_MILLI_SECOND;
        mAlarmManager.set(AlarmManager.RTC, timeReminderMillis, pendingIntent);
    }
}
