package hcmut.cse.ttcnpm.group13.restaurantbookingonline.UI;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by dinh on 12/1/2015.
 */
public class CustomEditText extends AppCompatEditText {

    Context context;

    private Boolean isMyFocusing = false;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // User has pressed Back key. So hide the keyboard
            InputMethodManager mgr = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(this.getWindowToken(), 0);
            this.setCursorVisible(false);
            this.setSelection(0);

            isMyFocusing = false;
            // TODO: Hide your view as you do it in your activity
        }
        return true;
    }

    @Override
    public void onEditorAction(int actionCode) {
        super.onEditorAction(actionCode);
        if (actionCode == EditorInfo.IME_ACTION_DONE)
        {
            this.setCursorVisible(false);
            this.setSelection(0);
            isMyFocusing = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        if (action == MotionEvent.ACTION_UP)
        {
            if (isMyFocusing == false) {
                isMyFocusing = true;
                this.setCursorVisible(true);
                this.clearFocus();
            }
        }
        return super.onTouchEvent(event);
    }
}

