package hcmut.cse.ttcnpm.group13.restaurantbookingonline.base;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.utils.LocaleUtils;

/**
 * Created by dinh on 11/13/2015.
 */
public abstract class MyFeatureActivity extends AppCompatActivity {

    public abstract void setMyFeaturesTitle();

    public abstract void setMyFeaturesIcon();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setMyFeaturesTitle();


        LocaleUtils.updateConfig(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            setMyFeaturesIcon();
        }
    }
}
