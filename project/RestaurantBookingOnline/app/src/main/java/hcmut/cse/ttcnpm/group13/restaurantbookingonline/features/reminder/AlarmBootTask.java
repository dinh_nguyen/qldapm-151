package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.booking.BookingSessionInfo;

/**
 * Created by dinh on 11/22/2015.
 */
public class AlarmBootTask implements Runnable {

    private final int MINUTE_TO_MILLI_SECOND = 60 * 1000;

    // index of list in coming booking session stored
    private ArrayList<Object> mListBooking;
    // android system alarm manager
    private AlarmManager mAlarmManager;

    private Context mContext;

    public AlarmBootTask(Context aContext, ArrayList<Object> mListBooking) {
        this.mContext = aContext;
        this.mAlarmManager = (AlarmManager) aContext.getSystemService(Context.ALARM_SERVICE);
        this.mListBooking = mListBooking;
    }

    @Override
    public void run() {
        for (int i = mListBooking.size() - 1; i >= 0; i--) {
            Intent intent = NotifyService.newIntentBoot(mContext, mListBooking, i);
            PendingIntent pendingIntent = PendingIntent.getService(mContext, i, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            BookingSessionInfo session = (BookingSessionInfo) mListBooking.get(i);
            long timeReminderMillis = session.getRealFullBookingDate().getTimeInMillis() - session.getRemindInfo().getValue() * MINUTE_TO_MILLI_SECOND;
            if (session != null) {
                mAlarmManager.set(AlarmManager.RTC, timeReminderMillis, pendingIntent);
            }
        }
    }
}
