package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by dinh on 11/22/2015.
 */
public class BootAlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.intent.action.BOOT_COMPLETED".equals(action) || "android.intent.action.QUICKBOOT_POWERON".equals(action)) {
            //android.os.Debug.waitForDebugger();
            Intent i = new Intent(context, AlarmBootService.class);
            context.startService(i);
        }
    }
}
