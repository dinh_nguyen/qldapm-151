package hcmut.cse.ttcnpm.group13.restaurantbookingonline.features.promotions;

import java.util.Calendar;
import java.util.UUID;

import hcmut.cse.ttcnpm.group13.restaurantbookingonline.R;

/**
 * Created by dinh on 11/15/2015.
 */
public class Promotion {
    private String mId;
    // image header and icon id
    private String mIconId;
    // image represent author
    private int mAuthorId = R.drawable.ic_restaurant_splash_screen;
    // link to this promotion
    private String mPromotionLink;
    // author write this news
    private String mAuthor;
    private String mTitle;
    private String mContent;
    private String mDescription;
    //
    private Calendar mDate;
    private Calendar mExpiredDate;
    //
    private boolean mIsNew;

    public Promotion() {
    }

    public Promotion(String aId,String aIconId, String aPromotionLink, String aAuthor, String aTitle, String aContent, String aDescription, Calendar aDate, Calendar aExpiredDate, boolean aIsNew) {
        mId = aId;
        mIconId = aIconId;
        mPromotionLink = aPromotionLink;
        mAuthor = aAuthor;
        mTitle = aTitle;
        mContent = aContent;
        mDescription = aDescription;
        mDate = aDate;
        mExpiredDate = aExpiredDate;
        mIsNew = aIsNew;
    }

    public String getPromotionLink() {
        return mPromotionLink;
    }

    public void setPromotionLink(String promotionLink) {
        mPromotionLink = promotionLink;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getId() {
        return mId;
    }

    public String getIconId() {
        return mIconId;
    }

    public void setIconId(String aIconId) {
        mIconId = aIconId;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Calendar getDate() {
        return mDate;
    }

    public void setDate(Calendar date) {
        mDate = date;
    }

    public Calendar getExpiredDate() {
        return mExpiredDate;
    }

    public void setExpiredDate(Calendar expiredDate) {
        mExpiredDate = expiredDate;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isNew() {
        return mIsNew;
    }

    public void setIsNew(boolean isNew) {
        this.mIsNew = isNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Promotion promotion = (Promotion) o;

        if (mAuthorId != promotion.mAuthorId) return false;
        if (mId != null ? !mId.equals(promotion.mId) : promotion.mId != null) return false;
        if (mIconId != null ? !mIconId.equals(promotion.mIconId) : promotion.mIconId != null)
            return false;
        if (mAuthor != null ? !mAuthor.equals(promotion.mAuthor) : promotion.mAuthor != null)
            return false;
        if (mTitle != null ? !mTitle.equals(promotion.mTitle) : promotion.mTitle != null)
            return false;
        if (mContent != null ? !mContent.equals(promotion.mContent) : promotion.mContent != null)
            return false;
        if (mDescription != null ? !mDescription.equals(promotion.mDescription) : promotion.mDescription != null)
            return false;
        if (mDate != null ? !mDate.equals(promotion.mDate) : promotion.mDate != null) return false;
        return !(mExpiredDate != null ? !mExpiredDate.equals(promotion.mExpiredDate) : promotion.mExpiredDate != null);

    }

    @Override
    public int hashCode() {
        int result = mId != null ? mId.hashCode() : 0;
        result = 31 * result + (mIconId != null ? mIconId.hashCode() : 0);
        result = 31 * result + mAuthorId;
        result = 31 * result + (mAuthor != null ? mAuthor.hashCode() : 0);
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        result = 31 * result + (mContent != null ? mContent.hashCode() : 0);
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        result = 31 * result + (mDate != null ? mDate.hashCode() : 0);
        result = 31 * result + (mExpiredDate != null ? mExpiredDate.hashCode() : 0);
        return result;
    }
}
