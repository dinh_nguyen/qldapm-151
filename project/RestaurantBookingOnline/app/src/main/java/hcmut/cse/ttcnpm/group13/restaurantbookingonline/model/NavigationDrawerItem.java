package hcmut.cse.ttcnpm.group13.restaurantbookingonline.model;

/**
 * Created by dinh on 11/6/2015.
 */
public class NavigationDrawerItem {
    private String title;
    private int icon;
    private int count = 0;
    // boolean to set visibility of the counter
    private boolean isCounterVisible = false;

    public NavigationDrawerItem() {
    }

    public NavigationDrawerItem(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public NavigationDrawerItem(String title, int icon, boolean isCounterVisible, int count) {
        this.title = title;
        this.icon = icon;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void updateCounterVisible() {
        if (this.count <= 0) {
            setIsCounterVisible(false);
        }
        else
        {
            setIsCounterVisible(true);
        }
    }

    public boolean isCounterVisible() {
        return isCounterVisible;
    }

    public void setIsCounterVisible(boolean isCounterVisible) {
        this.isCounterVisible = isCounterVisible;
    }
}
